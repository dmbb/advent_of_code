package main

import (
	"fmt"
	"io/ioutil"
	"strings"
)

func main() {
	dat, _ := ioutil.ReadFile("../input.txt")
	input := strings.Split((string(dat)), "\n")

	grid := make([][]rune, 0)

	x := 0
	y := 0
	dirx := 0
	diry := 0
	for i, line := range input {
		grid = append(grid, []rune(line))
		if i == 0 {
			for j, r := range grid[i] {
				if r == '|' {
					diry = 1
					dirx = 0
					x = j
					y = i
				}
			}
		}
	}

	done := false
	for {
		if done {
			break
		}
		switch grid[y][x] {
		case '|':
			if diry != 0 {
				y += diry
			} else {
				x += dirx
			}
		case '+':
			if dirx != 0 {
				dirx = 0
				if grid[y+1][x] != ' ' {
					diry = 1
				} else {
					diry = -1
				}
			} else {
				diry = 0
				if grid[y][x+1] != ' ' {
					dirx = 1
				} else {
					dirx = -1
				}
			}
			x += dirx
			y += diry
		case '-':
			if dirx != 0 {
				x += dirx
			} else {
				y += diry
			}
		case ' ':
			done = true
		default:
			fmt.Print(string(grid[y][x]))
			x += dirx
			y += diry
		}
	}
}
