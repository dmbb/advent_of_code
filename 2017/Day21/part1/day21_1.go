package main

import (
	"fmt"
	"io/ioutil"
	"math"
	"strings"
)

var m map[string]string

func main() {
	dat, _ := ioutil.ReadFile("../input.txt")
	input := strings.Split((string(dat)), "\n")
	init := ".#./..#/###"

	m = make(map[string]string)

	for _, line := range input {
		rule := strings.Split(line, "=>")
		rule[0] = strings.TrimSpace(rule[0])
		rule[1] = strings.TrimSpace(rule[1])

		m[rule[0]] = rule[1]

		s := convertSlice(rule[0])
		orig := convertSlice(rule[0])

		n := len(orig)
		for c := 0; c < 2; c++ {
			// Rotate
			for t := 0; t < 4; t++ {
				for i := 0; i < n; i++ {
					for j := 0; j < n; j++ {
						s[i][j] = orig[n-j-1][i]
					}
				}
				for i := range s {
					orig[i] = make([]string, len(s[i]))
					copy(orig[i], s[i])
				}
				m[convert(orig)] = rule[1]
			}

			//Mirror
			for i := 0; i < len(orig); i++ {
				orig[0][i] = s[len(orig)-1][i]
				orig[len(orig)-1][i] = s[0][i]
			}
		}
	}

	val := init
	print(val)

	for w := 0; w < 5; w++ {
		s := convertSlice(val)
		size := length(val)

		var news []string

		if size%2 == 0 {
			for ny := 0; ny < size/2; ny++ {
				for nx := 0; nx < size/2; nx++ {
					str := ""
					for y := 0; y < 2; y++ {
						for x := 0; x < 2; x++ {
							str += s[y+(ny*2)][x+(nx*2)]
						}
						str += "/"
					}
					news = append(news, strings.TrimSuffix(str, "/"))
				}
			}
		} else if size%3 == 0 {
			for ny := 0; ny < size/3; ny++ {
				for nx := 0; nx < size/3; nx++ {
					str := ""
					for y := 0; y < 3; y++ {
						for x := 0; x < 3; x++ {
							str += s[y+(ny*3)][x+(nx*3)]
						}
						str += "/"
					}
					news = append(news, strings.TrimSuffix(str, "/"))
				}
			}
		}
		val = combine(news)
		print(val)
		fmt.Println(strings.Count(val, "#"))
	}
}

func combine(s []string) string {
	l := int(math.Sqrt(float64(len(s))))
	for i, group := range s {
		s[i] = m[group]
	}
	out := ""

	for n := 0; n < l; n++ {
		for q := 0; q < length(s[0]); q++ {
			for i := 0; i < l; i++ {
				str := strings.Split(s[i+(n*l)], "/")
				out += str[q]
			}
			out += "/"
		}
	}
	return out
}

func convert(s [][]string) string {
	str := ""
	for _, row := range s {
		for _, char := range row {
			str += char
		}
		str += "/"
	}
	str = strings.TrimSuffix(str, "/")
	return str
}

func convertSlice(str string) [][]string {
	var s [][]string
	match := strings.Split(str, "/")
	for y, row := range match {
		s = append(s, make([]string, 0))
		for _, char := range row {
			s[y] = append(s[y], string(char))
		}
	}
	return s
}

func print(str string) {
	s := strings.Split(str, "/")
	for _, line := range s {
		fmt.Println(line)
	}
}

func length(str string) int {
	return strings.Index(str, "/")
}
