package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

func main() {
	dat, _ := ioutil.ReadFile("../input.txt")
	s := (string(dat))
	ele := strings.Split(s, ",")

	var lengths []int
	for _, element := range ele {
		num, _ := strconv.Atoi(element)
		lengths = append(lengths, num)
	}

	var list []int

	for i := 0; i < 256; i++ {
		list = append(list, i)
	}

	current, skip := 0, 0

	for _, length := range lengths {
		index := current
		var section []int

		for i := 0; i < length; i++ {
			section = append(section, list[index])
			index = (index + 1) % len(list)
		}
		//fmt.Printf("C: %v  S: %v  L: %v\n", current, skip, length)
		//fmt.Println(list)
		//fmt.Println(section)
		section = reverse(section)

		index = current
		for _, item := range section {
			list[index] = item
			index = (index + 1) % len(list)
		}

		current = (current + length + skip) % len(list)
		skip++
	}
	fmt.Println(list)
	fmt.Println(list[0] * list[1])
}

func reverse(list []int) []int {
	rev := make([]int, len(list))
	for i, num := range list {
		rev[len(list)-1-i] = num
	}
	return rev
}
