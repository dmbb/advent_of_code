package main

import (
	"fmt"
	"io/ioutil"
)

func main() {
	dat, _ := ioutil.ReadFile("../input.txt")
	r := []rune(string(dat))

	sum := 0
	size := len(r) - 1
	offset := 1
	for i, char := range r {
		if char == r[(i+offset)%size] {
			sum += int((char - '0'))
		}
	}
	fmt.Println(sum)
}
