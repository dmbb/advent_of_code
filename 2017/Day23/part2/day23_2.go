package main

import (
	"fmt"
	"math"
)

func main() {
	a := 0
	b := 0
	c := 0
	g := 0
	h := 0

	b = 81
	c = b

	if a != 0 {
		b = b * 100
		b += 100000
		c = b
		c += 17000
	}

	b = 108100
	c = 125100

	for {
		/*
			for {
				e = 2
				for {
					g = d
					g = g * e
					g -= b

					if g == 0 {
						f = 0
					}

					e += 1
					g = e
					g -= b
					if g == 0 {
						break
					}
				}

				d = d + 1
				g = d
				g -= b
				if g == 0 {
					break
				}
			}
		*/
		if !IsPrimeSqrt(b) {
			h += 1
			fmt.Println(h)
		}

		g = b
		g -= c

		if g == 0 {
			break
		}

		b += 17
	}
	fmt.Println(h)
}

func IsPrimeSqrt(value int) bool {
	for i := 2; i <= int(math.Floor(math.Sqrt(float64(value)))); i++ {
		if value%i == 0 {
			return false
		}
	}
	return value > 1
}
