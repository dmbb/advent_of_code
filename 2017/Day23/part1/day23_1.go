package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

func main() {
	dat, _ := ioutil.ReadFile("../input.txt")
	input := strings.Split((string(dat)), "\n")
	program(input)

}

func program(input []string) {
	registers := make(map[string]int)
	defer func() {
		if r := recover(); r != nil {
			fmt.Printf("h is %v\n", registers["h"])
		}
	}()
	i := 0
	end := len(input)
	registers["a"] = 1
	registers["b"] = 0
	registers["c"] = 0
	registers["d"] = 0
	registers["e"] = 0
	registers["f"] = 0
	registers["g"] = 0
	registers["h"] = 0

	prev := 0
	for {
		if registers["h"] != prev {
			prev = registers["h"]
			fmt.Println(prev)
		}
		inst := strings.Fields(input[i])
		switch inst[0] {
		case "set":
			if inst[2][0] >= 'a' && inst[2][0] <= 'z' {
				registers[inst[1]] = registers[inst[2]]
			} else {
				num, _ := strconv.Atoi(inst[2])
				registers[inst[1]] = num
			}
		case "sub":
			if inst[2][0] >= 'a' && inst[2][0] <= 'z' {
				registers[inst[1]] -= registers[inst[2]]
			} else {
				num, _ := strconv.Atoi(inst[2])
				registers[inst[1]] -= num
			}
		case "mul":
			if inst[2][0] >= 'a' && inst[2][0] <= 'z' {
				registers[inst[1]] *= registers[inst[2]]
			} else {
				num, _ := strconv.Atoi(inst[2])
				registers[inst[1]] *= num
			}
		case "jnz":
			var num int
			if inst[2][0] >= 'a' && inst[2][0] <= 'z' {
				num = registers[inst[2]]
			} else {
				num, _ = strconv.Atoi(inst[2])
			}

			d := 0
			if inst[1][0] >= 'a' && inst[1][0] <= 'z' {
				d = registers[inst[1]]
			} else {
				d, _ = strconv.Atoi(inst[1])
			}
			//fmt.Printf("%v jnz %v %v\n", i, d, num)
			if d != 0 {
				i += num
				continue
			}
		}
		i++
		if i == end {
			break
		}
	}
}
