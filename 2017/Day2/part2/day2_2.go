package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func main() {
	dat, err := ioutil.ReadFile("../input.txt")
	check(err)
	s := string(dat)

	fmt.Println(s)
	str := strings.Split(s, "\n")
	var sum int
	for _, line := range str {

		var division int
		numStr := strings.Fields(line)

		for i, numS := range numStr {
			num1, _ := strconv.Atoi(numS)
			for j, numS2 := range numStr {
				num2, _ := strconv.Atoi(numS2)

				if i == j {
					continue
				}

				if num1%num2 == 0 {
					division = num1 / num2
				}

				if num2%num1 == 0 {
					division = num2 / num1
				}
			}
		}
		sum += division
	}
	fmt.Println(sum)
}
