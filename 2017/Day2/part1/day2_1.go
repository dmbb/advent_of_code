package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func main() {
	dat, err := ioutil.ReadFile("../input.txt")
	check(err)
	s := string(dat)

	fmt.Println(s)
	str := strings.Split(s, "\n")
	var sum int
	for _, line := range str {

		var min, max int
		numStr := strings.Fields(line)

		for _, numS := range numStr {
			num, _ := strconv.Atoi(numS)

			if num < min || min == 0 {
				min = num
			}
			if num > max {
				max = num
			}
		}
		fmt.Println(min)
		fmt.Println(max)
		sum += max - min
	}

	fmt.Println(sum)
}
