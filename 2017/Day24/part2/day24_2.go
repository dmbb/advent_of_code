package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

type segment struct {
	front, back int
}

func main() {
	dat, _ := ioutil.ReadFile("../input.txt")
	input := strings.Split((string(dat)), "\n")

	var segments []segment
	for _, line := range input {
		s := strings.Split(line, "/")
		front, _ := strconv.Atoi(s[0])
		back, _ := strconv.Atoi(s[1])
		segments = append(segments, segment{front, back})
	}

	var combos [][]segment
	for i, seg := range segments {
		if seg.front != 0 {
			continue
		}
		used := make(map[int]int)
		used[i] = 1

		var bridge []segment
		bridge = append(bridge, seg)

		all := getBridges(used, bridge, segments)
		combos = append(combos, all...)
	}

	index := -1
	strength := 0
	length := 0
	for i, combo := range combos {
		l := len(combo)

		str := 0
		for _, seg := range combo {
			str += seg.front + seg.back
		}

		if l == length {
			if str > strength {
				strength = str
				length = l
				index = i
			}
		} else if l > length {
			strength = str
			length = l
			index = i
		}
	}
	fmt.Println(combos[index])
	fmt.Println(strength)
	fmt.Println(length)
}

func getBridges(used map[int]int, bridge []segment, segments []segment) [][]segment {
	var all [][]segment
	last := bridge[len(bridge)-1]
	matches, newSegments := getMatches(used, last, segments)

	for _, match := range matches {
		newUsed := make(map[int]int)
		for k, v := range used {
			newUsed[k] = v
		}
		newUsed[match] = 1

		new := make([]segment, len(bridge))
		copy(new, bridge)
		new = append(new, newSegments[match])

		combos := getBridges(newUsed, new, newSegments)
		all = append(all, combos...)
	}

	if len(matches) == 0 {
		all = append(all, bridge)
	}

	return all
}

func getMatches(used map[int]int, initial segment, segments []segment) ([]int, []segment) {
	var matches []int
	newSegments := make([]segment, len(segments))
	copy(newSegments, segments)

	for i, seg := range newSegments {
		if used[i] != 1 {
			if initial.back == seg.front {
				matches = append(matches, i)
			} else if initial.back == seg.back {
				newSegments[i].front = seg.back
				newSegments[i].back = seg.front
				matches = append(matches, i)
			}
		}
	}

	return matches, newSegments
}
