package main

import (
	"fmt"
	"io/ioutil"
	"math"
	"strings"
)

func main() {
	dat, _ := ioutil.ReadFile("../test.txt")
	s := (string(dat))
	dir := strings.Split(s, ",")

	x, z := 0, 0
	farthest := float64(0)

	for _, d := range dir {
		switch d {
		case "ne":
			x++

		case "sw":
			x--

		case "se":
			z--
			x++

		case "nw":
			z++
			x--

		case "s":
			z--

		case "n":
			z++
		}

		y := -x - z

		fx := float64(x)
		fy := float64(y)
		fz := float64(z)

		test := ((math.Abs(fx) + math.Abs(fy) + math.Abs(fz)) / 2)
		if test > farthest {
			farthest = test
		}
	}
	fmt.Println(farthest)
}
