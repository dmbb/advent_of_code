package main

import (
	"fmt"
	"io/ioutil"
	"strings"
)

func main() {

	dat, _ := ioutil.ReadFile("../input.txt")

	s := (string(dat))
	lines := strings.Split(s, "\n")

	valid := 0
	for _, line := range lines {
		words := strings.Split(line, " ")
		good := true
		for i, word := range words {
			for j, comparison := range words {
				if i == j {
					continue
				}
				for _, ana := range anagrams(comparison) {
					if word == ana {
						good = false
						break
					}
				}
				if !good {
					break
				}
			}
			if !good {
				break
			}
			//fmt.Printf("%v is %v", i, good)
		}
		if good {
			valid++
		}
	}
	fmt.Println(valid)
}

func merge(ins []rune, c rune) (result []string) {
	for i := 0; i <= len(ins); i++ {
		result = append(result, string(ins[:i])+string(c)+string(ins[i:]))
	}
	return
}

func anagrams(input string) []string {
	if len(input) == 1 {
		return []string{input}
	}

	runes := []rune(input)
	subPermutations := anagrams(string(runes[0 : len(input)-1]))

	result := []string{}
	for _, s := range subPermutations {
		result = append(result, merge([]rune(s), runes[len(input)-1])...)
	}

	return result
}
