package main

import (
	"fmt"
	"io/ioutil"
	"strings"
)

func main() {

	dat, _ := ioutil.ReadFile("../input.txt")

	s := (string(dat))
	lines := strings.Split(s, "\n")

	valid := 0
	for _, line := range lines {
		m := make(map[string]int)
		words := strings.Split(line, " ")
		good := true
		for _, word := range words {
			if m[word] == 0 {
				m[word] = 1
			} else {
				good = false
				break
			}
		}
		if good == true {
			valid++
		}
	}
	fmt.Println(valid)
}
