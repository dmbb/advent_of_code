package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

func main() {
	dat, _ := ioutil.ReadFile("../input.txt")
	s := string(dat)
	sl := strings.Split(s, "\n")

	instructions := make([]int, 0, 0)
	for _, e := range sl {
		conv, _ := strconv.Atoi(e)
		instructions = append(instructions, conv)
	}

	jump, i := 0, 0
	for {
		jump++
		prev := i
		i += instructions[i]
		instructions[prev]++

		if i >= len(instructions) {
			fmt.Println(jump)
			break
		}
	}
}
