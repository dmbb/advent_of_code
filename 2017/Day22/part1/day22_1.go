package main

import (
	"fmt"
	"io/ioutil"
	"strings"
)

var m map[string]string

func main() {
	dat, _ := ioutil.ReadFile("../input.txt")
	input := strings.Split((string(dat)), "\n")

	m = make(map[string]string)

	for y, line := range input {
		for x, char := range line {
			if string(char) == "#" {
				m[form(x-(len(line)/2), (len(line)/2)-y)] = string(char)
			}
		}
	}

	lx, ly := 0, 0
	d := 0
	count := 0
	for i := 0; i < 10000; i++ {
		s := form(lx, ly)
		if m[s] == "#" {
			d = (d + 1) % 4
			m[s] = ""
		} else {
			d = (d - 1)
			if d == -1 {
				d = 3
			}
			m[s] = "#"
			count++
		}

		switch d {
		case 0:
			ly++
		case 1:
			lx++
		case 2:
			ly--
		case 3:
			lx--
		}
	}
	fmt.Println(count)

}

func form(x, y int) string {
	return fmt.Sprintf("%v,%v", x, y)
}
