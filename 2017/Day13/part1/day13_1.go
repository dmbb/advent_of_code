package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

func main() {
	dat, _ := ioutil.ReadFile("../input.txt")
	s := strings.Split(string(dat), "\n")

	depth := make(map[int]int)

	for _, line := range s {
		f := strings.Fields(line)
		i, _ := strconv.Atoi(f[1])
		k, _ := strconv.Atoi(f[0][:len(f[0])-1])
		depth[k] = (i - 1) * 2
	}

	severity := 0
	for index := 0; index <= 88; {
		if depth[index] != 0 && (index)%depth[index] == 0 {
			severity = severity + index*((depth[index]/2)+1)
		}
		index++
	}
	fmt.Println(severity)
}
