package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

func main() {
	dat, _ := ioutil.ReadFile("../input.txt")
	s := strings.Split(string(dat), "\n")

	depth := make(map[int]int)

	for _, line := range s {
		f := strings.Fields(line)
		i, _ := strconv.Atoi(f[1])
		k, _ := strconv.Atoi(f[0][:len(f[0])-1])
		depth[k] = (i - 1) * 2
	}

	delay := -1
	for {
		delay++
		caught := false
		for index := 0; index <= 88; {
			caught = false
			if depth[index] != 0 && (delay+index)%depth[index] == 0 {
				caught = true
				break
			}
			index++
		}
		if !caught {
			fmt.Println(delay)
		}
	}
}
