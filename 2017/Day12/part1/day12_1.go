package main

import (
	"fmt"
	"io/ioutil"
	"strings"
)

type node struct {
	name        string
	strChildren []string
	children    []*node
}

var included map[string]string

func main() {
	included = make(map[string]string)
	dat, _ := ioutil.ReadFile("../input.txt")
	s := strings.Split(string(dat), "\n")

	m := make(map[string]*node)

	for _, line := range s {
		f := strings.Fields(line)
		n := node{}
		n.name = f[0]

		for _, ele := range f[2:len(f)] {
			ele = strings.TrimSuffix(ele, ",")
			n.strChildren = append(n.strChildren, ele)
		}

		m[f[0]] = &n

	}

	for _, v := range m {
		for _, child := range v.strChildren {
			v.children = append(v.children, m[child])
		}
	}

	count := 0
	for len(included) < len(m) {
		for k := range m {
			if included[k] == "" {
				count++
				(recurse(m[k]))
			}
		}
	}
	fmt.Println(count)
}

func recurse(n *node) int {
	included[n.name] = n.name
	total := 0
	for _, node := range n.children {
		if included[node.name] == "" {
			total += recurse(node)
		}
	}
	return total + 1
}

func contains(s []int, e int) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}
