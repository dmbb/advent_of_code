package main

import (
	"fmt"
	"io/ioutil"
)

func main() {
	dat, _ := ioutil.ReadFile("../test.txt")
	s := (string(dat))

	var lengths []int
	for _, element := range s {
		lengths = append(lengths, int(element))
	}
	lengths = append(lengths, 17)
	lengths = append(lengths, 31)
	lengths = append(lengths, 73)
	lengths = append(lengths, 47)
	lengths = append(lengths, 23)

	var list []int

	for i := 0; i < 256; i++ {
		list = append(list, i)
	}

	current, skip := 0, 0

	for round := 0; round < 64; round++ {
		for _, length := range lengths {
			index := current
			var section []int

			for i := 0; i < length; i++ {
				section = append(section, list[index])
				index = (index + 1) % len(list)
			}
			//fmt.Printf("C: %v  S: %v  L: %v\n", current, skip, length)
			//fmt.Println(list)
			//fmt.Println(section)
			section = reverse(section)

			index = current
			for _, item := range section {
				list[index] = item
				index = (index + 1) % len(list)
			}

			current = (current + length + skip) % len(list)
			skip++
		}
	}

	hash := make([]int, 16)
	hashi := -1
	for i, num := range list {
		if i%16 == 0 {
			hashi++
			if hashi == 16 {
				break
			}
			hash[hashi] = num
			continue
		}
		hash[hashi] = hash[hashi] ^ num
	}

	for _, h := range hash {
		fmt.Printf("%02x", h)
	}
}

func reverse(list []int) []int {
	rev := make([]int, len(list))
	for i, num := range list {
		rev[len(list)-1-i] = num
	}
	return rev
}
