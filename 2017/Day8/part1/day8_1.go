package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

type node struct {
	name        string
	weight      int
	parent      *node
	strChildren []string
	children    []*node
}

func main() {
	dat, _ := ioutil.ReadFile("../input.txt")
	s := string(dat)
	sl := strings.Split(s, "\n")

	registers := make(map[string]int)

	for _, line := range sl {
		split := strings.Split(line, "if")

		interpreter := strings.Fields(split[0])
		str := strings.Fields(split[1])

		if problem(str, registers) {
			num, _ := strconv.Atoi(interpreter[2])
			if interpreter[1] == "inc" {
				registers[interpreter[0]] += num
			} else if interpreter[1] == "dec" {
				registers[interpreter[0]] -= num
			}

		}
	}
	fmt.Println(largest(registers))
}

func problem(str []string, registers map[string]int) bool {
	num, _ := strconv.Atoi(str[2])
	switch str[1] {
	case ">":
		return registers[str[0]] > num
	case "<":
		return registers[str[0]] < num
	case ">=":
		return registers[str[0]] >= num
	case "==":
		return registers[str[0]] == num
	case "<=":
		return registers[str[0]] <= num
	case "!=":
		return registers[str[0]] != num
	default:
		panic(str[1])
	}
}

func largest(m map[string]int) int {
	var large int
	index := 0
	for _, v := range m {
		if index == 0 {
			large = v
		}

		if v > large {
			large = v
		}
		index++
	}
	return large
}
