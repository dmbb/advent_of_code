package main

import "fmt"

func main() {
	var state rune
	state = 'A'

	tape := make(map[int64]int)
	var pos int64
	pos = 0

	for i := 0; i < 12386363; i++ {
		switch state {
		case 'A':
			if tape[pos] == 0 {
				tape[pos] = 1
				pos++
				state = 'B'
			} else {
				tape[pos] = 0
				pos--
				state = 'E'
			}
		case 'B':
			if tape[pos] == 0 {
				tape[pos] = 1
				pos--
				state = 'C'
			} else {
				tape[pos] = 0
				pos++
				state = 'A'
			}
		case 'C':
			if tape[pos] == 0 {
				tape[pos] = 1
				pos--
				state = 'D'
			} else {
				tape[pos] = 0
				pos++
				state = 'C'
			}
		case 'D':
			if tape[pos] == 0 {
				tape[pos] = 1
				pos--
				state = 'E'
			} else {
				tape[pos] = 0
				pos--
				state = 'F'
			}
		case 'E':
			if tape[pos] == 0 {
				tape[pos] = 1
				pos--
				state = 'A'
			} else {
				tape[pos] = 1
				pos--
				state = 'C'
			}
		case 'F':
			if tape[pos] == 0 {
				tape[pos] = 1
				pos--
				state = 'E'
			} else {
				tape[pos] = 1
				pos++
				state = 'A'
			}
		}
	}

	count := 0
	for _, v := range tape {
		if v == 1 {
			count++
		}
	}
	fmt.Println(count)
}
