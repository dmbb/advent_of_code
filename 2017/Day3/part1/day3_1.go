package main

import (
	"fmt"
	"math"
)

const input = 368078

func main() {
	tmp := math.Ceil(math.Pow(input, 0.5))
	power := int(tmp) + ((int(tmp) + 1) % 2)
	ring := (power - 1) / 2
	dist := math.Abs(float64((int(math.Pow(float64(power), 2)-input) % (power - 1)) - ring))
	fmt.Println(int(dist) + ring)
}
