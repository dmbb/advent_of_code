package main

import (
	"fmt"
	"strconv"
)

const input = 368078

func main() {
	storage := make(map[string]int)
	storage["0,0"] = 1

	x, y, d, m := 0, 0, 1, 1

	for i := 0; i < 30; i++ {
		for (2 * x * d) < m {
			str := formString(x, y)
			storage[str] = sumNeighbours(x, y, storage)
			if storage[str] > input {
				fmt.Printf("%v, %v = %v\n", x, y, storage[str])
				return
			}
			x = x + d
		}
		for (2 * y * d) < m {
			str := formString(x, y)
			storage[str] = sumNeighbours(x, y, storage)
			if storage[str] > input {
				fmt.Printf("%v, %v = %v\n", x, y, storage[str])
				return
			}
			y = y + d
		}
		d = -1 * d
		m = m + 1
	}
}

func sumNeighbours(x, y int, storage map[string]int) int {
	sum := 0
	for tx := x - 1; tx <= x+1; tx++ {
		for ty := y - 1; ty <= y+1; ty++ {
			sum += storage[formString(x, y)]
		}
	}
	return sum
}

func formString(x, y int) string {
	strx := strconv.Itoa(x)
	stry := strconv.Itoa(y)
	return strx + "," + stry
}
