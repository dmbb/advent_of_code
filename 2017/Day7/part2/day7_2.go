package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

type node struct {
	name        string
	weight      int
	parent      *node
	strChildren []string
	children    []*node
}

func main() {
	dat, _ := ioutil.ReadFile("../input.txt")
	s := string(dat)
	sl := strings.Split(s, "\n")

	m := make(map[string]*node)

	var root *node
	for _, line := range sl {
		fields := strings.Fields(line)
		n := node{}
		n.name = fields[0]
		n.weight, _ = strconv.Atoi(fields[1][1 : len(fields[1])-1])

		m[n.name] = &n

		if len(fields) < 3 {
			continue
		}
		for _, child := range fields[3:] {
			child = strings.TrimSuffix(child, ",")
			n.strChildren = append(n.strChildren, child)
		}
	}

	for _, v := range m {
		for _, child := range v.strChildren {
			c := m[child]
			v.children = append(v.children, c)
			c.parent = v
		}
	}
	for _, v := range m {
		if v.parent == nil {
			fmt.Println(v.name)
			root = v
		}
	}

	//total := getBalance(root)
	w, correct := wrong(root)
	fmt.Println("\n")
	fmt.Print(w.name)
	fmt.Print(" ")
	fmt.Print(w.weight)
	fmt.Print(" ")
	fmt.Println(correct)
}

func wrong(n *node) (*node, int) {
	w := wrongNode(n)
	smallest := 999999999
	largest := 0

	for _, child := range n.children {
		weight := getBalance(child)
		if weight < smallest {
			smallest = weight
		}
		if weight > largest {
			largest = weight
		}
	}
	return w, w.weight - (largest - smallest)

}

func wrongNode(n *node) *node {
	if len(n.children) == 0 {
		return n
	}
	var weights []int
	for _, child := range n.children {
		weight := getBalance(child)
		weights = append(weights, weight)
	}

	equality, largest, index := 0, 0, 0
	for _, w := range weights {
		equality += w
	}
	equality = equality / len(weights)

	for i, w := range weights {
		if w == equality { // This node is the problem.
			return n
		}
		if w > largest {
			largest = w
			index = i
		}
	}
	return wrongNode(n.children[index])
}

func getBalance(n *node) int {
	total := n.weight
	for _, child := range n.children {
		total += getBalance(child)
	}
	return total
}
