package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

type node struct {
	name        string
	weight      int
	parent      *node
	strChildren []string
	children    []*node
}

func main() {
	dat, _ := ioutil.ReadFile("../input.txt")
	s := string(dat)
	sl := strings.Split(s, "\n")

	m := make(map[string]*node)

	var root *node
	for _, line := range sl {
		fields := strings.Fields(line)
		n := node{}
		n.name = fields[0]
		n.weight, _ = strconv.Atoi(fields[1][1 : len(fields[1])-1])

		m[n.name] = &n

		if len(fields) < 3 {
			continue
		}
		for _, child := range fields[3:] {
			child = strings.TrimSuffix(child, ",")
			n.strChildren = append(n.strChildren, child)
		}
	}

	for _, v := range m {
		for _, child := range v.strChildren {
			c := m[child]
			v.children = append(v.children, c)
			c.parent = v
		}
	}
	for _, v := range m {
		if v.parent == nil {
			fmt.Println(v.name)
			root = v
		}
	}
	fmt.Println(root)
}
