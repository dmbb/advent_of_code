package main

import (
	"fmt"
	"io/ioutil"

	"github.com/golang-collections/collections/stack"
)

func main() {
	dat, _ := ioutil.ReadFile("../k.txt")
	s := (string(dat))

	st := stack.New()

	count := 0
	square := 0
	garbage := false
	next := true
	for _, char := range s {
		if next {
			if char == rune('!') {
				next = false
			}
			if char == rune('<') {
				garbage = true
			}
			if char == rune('>') {
				garbage = false
			}
			if !garbage {
				if char == rune('{') {
					st.Push(char)
					square++
				}
				if char == rune('}') {
					for {
						check := st.Pop()
						if check == rune('{') {
							count += square
							square--
							break
						}
					}
				}
			}
		} else {
			next = true
		}
	}
	fmt.Println(count)
}
