package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

func main() {
	dat, _ := ioutil.ReadFile("../input.txt")
	input := (string(dat))
	inst := strings.Split(input, ",")

	m := make(map[string]int)

	m["a"] = 0
	m["b"] = 1
	m["c"] = 2
	m["d"] = 3
	m["e"] = 4
	m["f"] = 5
	m["g"] = 6
	m["h"] = 7
	m["i"] = 8
	m["j"] = 9
	m["k"] = 10
	m["l"] = 11
	m["m"] = 12
	m["n"] = 13
	m["o"] = 14
	m["p"] = 15

	seen := make(map[string]int)

	for i := 0; i < 1000; i++ {
		for _, in := range inst {
			switch in[0] {
			case 's':
				num, _ := strconv.Atoi(in[1:])
				for k, v := range m {
					m[k] = (v + num) % 16
				}
			case 'x':
				nums := strings.Split(in[1:], "/")
				first, _ := strconv.Atoi(nums[0])
				var f, s string
				second, _ := strconv.Atoi(nums[1])
				for k, v := range m {
					if v == first {
						f = k
					}
					if v == second {
						s = k
					}
				}
				m[f] = second
				m[s] = first
			case 'p':
				progs := strings.Split(in[1:], "/")
				f := m[progs[0]]
				s := m[progs[1]]

				m[progs[0]] = s
				m[progs[1]] = f
			}
		}
		if i%1000 == 0 {
			fmt.Println(i)
		}
		var out string
		for i := 0; i < 16; i++ {
			for k, v := range m {
				if v == i {
					out = out + k
				}
			}
		}
		if seen[out] != 0 {
			fmt.Printf("on %v i saw %s at %v\n", i, out, seen[out])
		} else {
			seen[out] = i
		}
	}
	fmt.Println(1000000000 % 30)

	/*
		fmt.Println(rune("a"[0]) - 97)
		inc := make(map[string]int)
		for k, v := range m {
			inc[k] = v - int((rune(k[0]) - 97))
			if inc[k] < 0 {
				inc[k] = inc[k] + 16
			}
			fmt.Printf("%s-started:%v at:%v\n", k, int((rune(k[0]) - 97)), v)
		}

		for k, v := range m {
			m[k] = (v + inc[k]) % 16
		}
	*/

}
