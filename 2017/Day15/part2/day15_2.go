package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

var facA int64
var facB int64

func main() {
	dat, _ := ioutil.ReadFile("../input.txt")
	input := (string(dat))

	s := strings.Fields(input)
	tA, _ := (strconv.Atoi(s[4]))
	tB, _ := (strconv.Atoi(s[9]))
	genA := int64(tA)
	genB := int64(tB)

	facA = int64(16807)
	facB = int64(48271)

	same := 0

	for i := 0; i < 5000000; i++ {
		genA = getNextA(genA)
		genB = getNextB(genB)

		if genA&65535 == genB&65535 {
			same++
		}
	}
	fmt.Println(same)
}

func getNextA(genA int64) int64 {
	for {
		genA = (genA * facA) % 2147483647
		if genA%4 == 0 {
			return genA
		}
	}
}

func getNextB(genB int64) int64 {
	for {
		genB = (genB * facB) % 2147483647
		if genB%8 == 0 {
			return genB
		}
	}
}
