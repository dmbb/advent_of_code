package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

func main() {
	dat, _ := ioutil.ReadFile("../input.txt")
	input := (string(dat))
	s := strings.Fields(input)

	tA, _ := (strconv.Atoi(s[4]))
	tB, _ := (strconv.Atoi(s[9]))
	genA := int64(tA)
	genB := int64(tB)
	facA := int64(16807)
	facB := int64(48271)

	same := 0

	for i := 0; i < 40000000; i++ {
		genA = (genA * facA) % 2147483647
		genB = (genB * facB) % 2147483647

		if genA&65535 == genB&65535 {
			same++
		}
	}
	fmt.Println(same)
}
