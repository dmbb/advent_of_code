package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

type particle struct {
	px, py, pz int
	vx, vy, vz int
	ax, ay, az int
}

func main() {
	dat, _ := ioutil.ReadFile("../input.txt")
	input := strings.Split((string(dat)), "\n")

	m := make(map[int]particle)

	for i, line := range input {
		line = strings.TrimSpace(line)
		var p particle
		parts := strings.Split(line, ", ")
		position := strings.Split(strings.TrimPrefix(parts[0][:len(parts[0])-1], "p=<"), ",")
		velocity := strings.Split(strings.TrimPrefix(parts[1][:len(parts[1])-1], "v=<"), ",")
		accel := strings.Split(strings.TrimPrefix(parts[2][:len(parts[2])-1], "a=<"), ",")
		p.px, _ = strconv.Atoi(position[0])
		p.py, _ = strconv.Atoi(position[1])
		p.pz, _ = strconv.Atoi(position[2])

		p.vx, _ = strconv.Atoi(velocity[0])
		p.vy, _ = strconv.Atoi(velocity[1])
		p.vz, _ = strconv.Atoi(velocity[2])

		p.ax, _ = strconv.Atoi(accel[0])
		p.ay, _ = strconv.Atoi(accel[1])
		p.az, _ = strconv.Atoi(accel[2])

		m[i] = p
	}

	fmt.Println(len(m))
	for e := 0; e < 1000; e++ {
		for k, v := range m {
			var p particle
			p.ax = v.ax
			p.ay = v.ay
			p.az = v.az

			p.vx = v.vx + v.ax
			p.vy = v.vy + v.ay
			p.vz = v.vz + v.az

			p.px = v.px + p.vx
			p.py = v.py + p.vy
			p.pz = v.pz + p.vz

			m[k] = p
		}

		for i, v := range m {
			var del []int
			for k, a := range m {
				if i == k {
					continue
				}
				if v.px == a.px && v.py == a.py && v.pz == a.pz {
					del = append(del, k)
				}
			}
			if len(del) > 0 {
				for _, d := range del {
					delete(m, d)
				}
				delete(m, i)
				fmt.Println(len(m))
			}
		}
	}
}
