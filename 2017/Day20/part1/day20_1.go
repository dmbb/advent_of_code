package main

import (
	"fmt"
	"io/ioutil"
	"math"
	"strconv"
	"strings"
)

type particle struct {
	px, py, pz int
	vx, vy, vz int
	ax, ay, az int
}

func main() {
	dat, _ := ioutil.ReadFile("../input.txt")
	input := strings.Split((string(dat)), "\n")

	m := make(map[int]particle)

	for i, line := range input {
		line = strings.TrimSpace(line)
		var p particle
		parts := strings.Split(line, ", ")
		position := strings.Split(strings.TrimPrefix(parts[0], "p=<"), ",")
		velocity := strings.Split(strings.TrimPrefix(parts[1], "v=<"), ",")
		accel := strings.Split(strings.TrimPrefix(parts[2][:len(parts[2])-1], "a=<"), ",")
		p.px, _ = strconv.Atoi(position[0])
		p.py, _ = strconv.Atoi(position[1])
		p.pz, _ = strconv.Atoi(position[2])

		p.vx, _ = strconv.Atoi(velocity[0])
		p.vy, _ = strconv.Atoi(velocity[1])
		p.vz, _ = strconv.Atoi(velocity[2])

		p.ax, _ = strconv.Atoi(accel[0])
		p.ay, _ = strconv.Atoi(accel[1])
		p.az, _ = strconv.Atoi(accel[2])

		m[i] = p
	}

	var accelmatching []int
	smallest := math.Inf(1)
	for k, v := range m {
		t := math.Abs(float64(v.ax)) + math.Abs(float64(v.ay)) + math.Abs(float64(v.az))

		if t < smallest {
			smallest = t
			accelmatching = []int{}
		}

		if t == smallest {
			accelmatching = append(accelmatching, k)
		}
	}

	var velmatching []int
	largest := float64(0)
	for _, k := range accelmatching {
		ax := float64(m[k].ax)
		ay := float64(m[k].ay)
		az := float64(m[k].az)
		vx := float64(m[k].vx)
		vy := float64(m[k].vy)
		vz := float64(m[k].vz)

		sx := (ay * vz) - (az * vy)
		sy := (az * vx) - (ax * vz)
		sz := (ax * vy) - (ay * vx)

		mag := math.Abs(float64(m[k].vx)) + math.Abs(float64(m[k].vy)) + math.Abs(float64(m[k].vz))

		fmt.Println(k)
		fmt.Println(mag)
		l := (ax * vx) + (ay * vy) + (az * vz)
		fmt.Println(l)
		if sx == sy && sy == sz && sz == 0 {
			l := (ax + vx) * (ay + vy) * (az + vz)
			fmt.Println(l)
			if l < 0 {
				mag := math.Abs(float64(m[k].vx)) + math.Abs(float64(m[k].vy)) + math.Abs(float64(m[k].vz))
				if mag > largest {
					largest = mag
					velmatching = []int{}
				}

				if mag == largest {
					velmatching = append(velmatching, k)
				}
			}
		}
	}

	index := -1
	smallest = math.Inf(1)
	for _, k := range velmatching {
		d := math.Abs(float64(m[k].px)) + math.Abs(float64(m[k].py)) + math.Abs(float64(m[k].pz))

		if d < smallest {
			smallest = d
			index = k
		}
	}
	fmt.Println(accelmatching)
	fmt.Println(velmatching)
	fmt.Println(index)
}
