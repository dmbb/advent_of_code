package main

import (
	"io/ioutil"
	"strconv"
	"strings"
)

func main() {
	dat, _ := ioutil.ReadFile("../input.txt")
	s := string(dat)
	sl := strings.Split(s, "\n")

	ints := make([]int, 0, 0)
	for _, e := range sl {
		conv, _ := strconv.Atoi(e)
		ints = append(ints, conv)
	}

}
