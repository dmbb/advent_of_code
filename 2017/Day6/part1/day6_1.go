package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

func main() {
	dat, _ := ioutil.ReadFile("../input.txt")
	s := string(dat)
	sl := strings.Fields(s)

	ints := make([]int, 0, 0)
	for _, e := range sl {
		conv, _ := strconv.Atoi(e)
		ints = append(ints, conv)
	}

	fmt.Println(ints)
	m := make(map[string]int)
	cycles := 0

	count := 0
	for {
		max := -1
		maxi := -1
		for i, v := range ints {
			if v > max {
				max = v
				maxi = i
			}
		}

		ints[maxi] = 0

		count++
		for max > 0 {
			max--
			maxi++
			if maxi >= len(ints) {
				maxi = 0
			}
			ints[maxi]++
		}
		fmt.Println(ints)
		str := formString(ints)
		fmt.Println(str)
		if m[str] > 0 {
			cycles = count - m[str]
			break
		}
		m[str] = count
	}
	fmt.Println(cycles)
}
func formString(ints []int) string {
	str := ""
	for _, x := range ints {
		str = str + strconv.Itoa(x) + ","
	}
	return str
}
