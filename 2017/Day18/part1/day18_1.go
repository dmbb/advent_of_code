package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

func main() {
	dat, _ := ioutil.ReadFile("../input.txt")
	input := strings.Split((string(dat)), "\n")

	c1 := make(chan int, 10000)
	c2 := make(chan int, 10000)
	c3 := make(chan int)

	go program(c1, c2, c3, input, 1)
	go program(c2, c1, c3, input, 0)

	<-c3
}

func program(send, receive, main chan int, input []string, id int) {
	count := 0
	defer func() {
		if r := recover(); r != nil {
			fmt.Printf("count is %v\n", count)
		}
	}()
	fmt.Printf("%v started \n", id)
	i := 0
	end := len(input)
	registers := make(map[string]int)
	registers["p"] = id

	for {
		inst := strings.Fields(input[i])
		switch inst[0] {
		case "snd":
			count++
			if inst[1][0] >= 'a' && inst[1][0] <= 'z' {
				send <- registers[inst[1]]
			} else {
				num, _ := strconv.Atoi(inst[1])
				send <- num
			}
			fmt.Printf("%v sent %v\n", id, count)
		case "rcv":
			registers[inst[1]] = <-receive
			//fmt.Printf("%v %v\n", id, registers)
		case "set":
			if inst[2][0] >= 'a' && inst[2][0] <= 'z' {
				registers[inst[1]] = registers[inst[2]]
			} else {
				num, _ := strconv.Atoi(inst[2])
				registers[inst[1]] = num
			}
		case "add":
			if inst[2][0] >= 'a' && inst[2][0] <= 'z' {
				registers[inst[1]] += registers[inst[2]]
			} else {
				num, _ := strconv.Atoi(inst[2])
				registers[inst[1]] += num
			}
		case "mul":
			if inst[2][0] >= 'a' && inst[2][0] <= 'z' {
				registers[inst[1]] *= registers[inst[2]]
			} else {
				num, _ := strconv.Atoi(inst[2])
				registers[inst[1]] *= num
			}
		case "mod":
			if inst[2][0] >= 'a' && inst[2][0] <= 'z' {
				registers[inst[1]] %= registers[inst[2]]
			} else {
				num, _ := strconv.Atoi(inst[2])
				registers[inst[1]] %= num
			}
		case "jgz":
			var num int
			if inst[2][0] >= 'a' && inst[2][0] <= 'z' {
				num = registers[inst[2]]
			} else {
				num, _ = strconv.Atoi(inst[2])
			}

			d := 0
			if inst[1][0] >= 'a' && inst[1][0] <= 'z' {
				d = registers[inst[1]]
			} else {
				d, _ = strconv.Atoi(inst[1])
			}
			if d > 0 {
				i += num
				continue
			}
		}
		i++
		if i == end {
			break
		}
	}
	main <- 1
}
