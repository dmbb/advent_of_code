package main

import (
	"fmt"
	"io/ioutil"
)

func main() {
	knotHash()
}

func knotHash() {
	dat, _ := ioutil.ReadFile("../input.txt")
	input := (string(dat))
	var hashes [][]int
	var rows []string
	grid := make([][]int, 128, 128)
	for i := 0; i < 128; i++ {
		rows = append(rows, fmt.Sprintf("%s-%v", input, i))
	}

	for _, s := range rows {
		var lengths []int
		for _, element := range s {
			lengths = append(lengths, int(element))
		}
		lengths = append(lengths, 17)
		lengths = append(lengths, 31)
		lengths = append(lengths, 73)
		lengths = append(lengths, 47)
		lengths = append(lengths, 23)

		var list []int

		for i := 0; i < 256; i++ {
			list = append(list, i)
		}

		current, skip := 0, 0

		for round := 0; round < 64; round++ {
			for _, length := range lengths {
				index := current
				var section []int

				for i := 0; i < length; i++ {
					section = append(section, list[index])
					index = (index + 1) % len(list)
				}
				//fmt.Printf("C: %v  S: %v  L: %v\n", current, skip, length)
				//fmt.Println(list)
				//fmt.Println(section)
				section = reverse(section)

				index = current
				for _, item := range section {
					list[index] = item
					index = (index + 1) % len(list)
				}

				current = (current + length + skip) % len(list)
				skip++
			}
		}

		hash := make([]int, 16)
		hashi := -1
		for i, num := range list {
			if i%16 == 0 {
				hashi++
				if hashi == 16 {
					break
				}
				hash[hashi] = num
				continue
			}
			hash[hashi] = hash[hashi] ^ num
		}

		/*
			for _, h := range hash {
				fmt.Printf("%02x", h)
			}
		*/
		hashes = append(hashes, hash)
	}
	total := 0

	for y, hash := range hashes {
		for _, char := range hash {
			for i := 0; i < 8; i++ {
				if char&128 == 128 {
					grid[y] = append(grid[y], 1)
					total++
				} else {
					grid[y] = append(grid[y], 0)
				}
				char = char << 1
			}
		}
	}
	fmt.Println(total)
	groups(grid)

}

func reverse(list []int) []int {
	rev := make([]int, len(list))
	for i, num := range list {
		rev[len(list)-1-i] = num
	}
	return rev
}

type node struct {
	name        string
	strChildren []string
	children    []*node
}

var included map[string]string

func groups(grid [][]int) {
	included = make(map[string]string)
	m := make(map[string]*node)

	for y, row := range grid {
		for x, val := range row {
			if val == 0 {
				continue
			}
			n := node{}
			n.name = fmt.Sprintf("%v,%v", y, x)

			if y != 0 {
				if grid[y-1][x] == 1 {
					n.strChildren = append(n.strChildren, fmt.Sprintf("%v,%v", y-1, x))
				}
			}
			if y != 127 {
				if grid[y+1][x] == 1 {
					n.strChildren = append(n.strChildren, fmt.Sprintf("%v,%v", y+1, x))
				}
			}
			if x != 0 {
				if grid[y][x-1] == 1 {
					n.strChildren = append(n.strChildren, fmt.Sprintf("%v,%v", y, x-1))
				}
			}
			if x != 127 {
				if grid[y][x+1] == 1 {
					n.strChildren = append(n.strChildren, fmt.Sprintf("%v,%v", y, x+1))
				}
			}

			m[n.name] = &n
		}
	}

	for _, v := range m {
		for _, child := range v.strChildren {
			v.children = append(v.children, m[child])
		}
	}

	count := 0
	for len(included) < len(m) {
		for k := range m {
			if included[k] == "" {
				count++
				(recurse(m[k]))
			}
		}
	}
	fmt.Println(count)
}

func recurse(n *node) int {
	included[n.name] = n.name
	total := 0
	for _, node := range n.children {
		if included[node.name] == "" {
			total += recurse(node)
		}
	}
	return total + 1
}

func contains(s []int, e int) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}
