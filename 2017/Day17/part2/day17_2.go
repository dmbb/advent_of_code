package main

import "fmt"

func main() {
	input := 343

	index := 0
	var buffer []int
	buffer = append(buffer, 0)
	for i := 1; i <= 50000000; i++ {
		index = (index+input)%i + 1
		if index == 1 {
			fmt.Println(i)
		}
	}
}
