package main

import "fmt"

func main() {
	input := 343

	index := 0
	var buffer []int
	buffer = append(buffer, 0)
	for i := 1; i <= 2017; i++ {
		index = (index+input)%i + 1
		buffer = append(buffer, 0)
		copy(buffer[index+1:], buffer[index:])
		buffer[index] = i
	}
	fmt.Println(buffer[index+1])
}
