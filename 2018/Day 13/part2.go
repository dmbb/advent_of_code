package main

import (
	"fmt"
	"io/ioutil"
	"sort"
	"strings"
)

type cart struct {
	y, x int
	dir  rune
	turn rune
	id   int
}

type cartSet []cart

func (c cartSet) Sort() {
	sort.Sort(c)
}

func (c cartSet) Len() int {
	return len(c)
}

func (c cartSet) Less(i, j int) bool {
	if c[i].y < c[j].y {
		return true
	} else if c[i].y == c[j].y {
		return c[i].x < c[j].x
	}
	return false
}

func (c cartSet) Swap(i, j int) {
	c[i], c[j] = c[j], c[i]
}

func main() {
	data, _ := ioutil.ReadFile("2018/Day 13/input.txt")
	input := string(data)
	lines := strings.Split(input, "\n")

	grid := make([][]rune, 0)
	carts := make(cartSet, 0)
	id := 0
	for y, line := range lines {
		grid = append(grid, make([]rune, 0))
		for x, char := range line {
			if char == '>' || char == '<' || char == 'v' || char == '^' {
				carts = append(carts, cart{x: x, y: y, dir: char, turn: 'l', id: id})
				id++
				if char == '>' || char == '<' {
					grid[y] = append(grid[y], '-')
				} else if char == '^' || char == 'v' {
					grid[y] = append(grid[y], '|')
				}
			} else {
				grid[y] = append(grid[y], char)
			}
		}
	}
	carts.Sort()

	ignore := make([]int, 0)
	for tick := 1; ; tick++ {
		carts.Sort()
		for i, c := range carts {
			skip := false
			for _, id := range ignore {
				if c.id == id {
					skip = true
				}
			}
			if skip {
				continue
			}
			switch c.dir {
			case '>':
				carts[i].x = carts[i].x + 1
				switch grid[carts[i].y][carts[i].x] {
				case '/':
					carts[i].dir = '^'
				case '\\':
					carts[i].dir = 'v'
				case '+':
					switch c.turn {
					case 'l':
						carts[i].dir = '^'
						carts[i].turn = 's'
					case 's':
						carts[i].turn = 'r'
					case 'r':
						carts[i].dir = 'v'
						carts[i].turn = 'l'
					}
				}
			case '<':
				carts[i].x = carts[i].x - 1
				switch grid[carts[i].y][carts[i].x] {
				case '/':
					carts[i].dir = 'v'
				case '\\':
					carts[i].dir = '^'
				case '+':
					switch c.turn {
					case 'l':
						carts[i].dir = 'v'
						carts[i].turn = 's'
					case 's':
						carts[i].turn = 'r'
					case 'r':
						carts[i].dir = '^'
						carts[i].turn = 'l'
					}
				}
			case 'v':
				carts[i].y = carts[i].y + 1
				switch grid[carts[i].y][carts[i].x] {
				case '/':
					carts[i].dir = '<'
				case '\\':
					carts[i].dir = '>'
				case '+':
					switch c.turn {
					case 'l':
						carts[i].dir = '>'
						carts[i].turn = 's'
					case 's':
						carts[i].turn = 'r'
					case 'r':
						carts[i].dir = '<'
						carts[i].turn = 'l'
					}
				}
			case '^':
				carts[i].y = carts[i].y - 1
				switch grid[carts[i].y][carts[i].x] {
				case '/':
					carts[i].dir = '>'
				case '\\':
					carts[i].dir = '<'
				case '+':
					switch c.turn {
					case 'l':
						carts[i].dir = '<'
						carts[i].turn = 's'
					case 's':
						carts[i].turn = 'r'
					case 'r':
						carts[i].dir = '>'
						carts[i].turn = 'l'
					}
				}
			}
			if len(carts)-len(ignore) == 1 {
				fmt.Printf("%d,%d\n", carts[i].x, carts[i].y)
				return
			}

			for j, c2 := range carts {
				if carts[i].x == c2.x && carts[i].y == c2.y && i != j {
					skip := false
					for _, id := range ignore {
						if c2.id == id {
							skip = true
						}
					}
					if skip {
						continue
					}
					fmt.Printf("%d,%d\n", carts[i].x, carts[i].y)
					ignore = append(ignore, c.id)
					ignore = append(ignore, c2.id)
				}
			}
		}
	}

}

/*
	for i, c1 := range carts {
		for j, c2 := range carts {
			if c1.x == c2.x && c1.y == c2.y && i != j{
				fmt.Printf("x:%d, y:%d\n", c1.x, c1.y)
				for y := range grid {
					for x := range grid[y] {
						cart := false
						for _, c := range carts {
							if c.x == x && c.y == y {
								if c1.x == c.x && c1.y == c.y {
									fmt.Printf("X")
									cart = true
									break
								}
								fmt.Printf("%c", c.dir)
								cart = true
								break
							}
						}
						if !cart {
							fmt.Printf("%c", grid[y][x])
						}
					}
					fmt.Print("\n")
				}
				return

			}
		}
	}
*/
