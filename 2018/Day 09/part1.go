package main

import (
	"container/ring"
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

func main() {

	data, _ := ioutil.ReadFile("2018/Day 09/input.txt")
	input := string(data)
	split := strings.Split(input, " ")
	players, _ := strconv.Atoi(split[0])
	last, _ := strconv.Atoi(split[6])

	score := make([]int, players)

	r := ring.New(1)

	r.Value = 0
	/*
		zero := r

		fmt.Print("(")
		fmt.Print(r.Value)
		fmt.Print(")")
		fmt.Println(" ")
	*/

	current := -1
	for i := 1; i < last; i++ {
		if current+2 == players {
			current = 0
		} else {
			current++
		}
		if i%23 == 0 {
			r = r.Move(-8)
			points := r.Next().Value.(int)
			score[current] += points + i
			r.Unlink(1)
			r = r.Next()
			continue
		}
		r = r.Move(1)
		n := ring.New(1)
		n.Value = i
		r.Link(n)
		r = r.Next()

		/*
			fmt.Printf("[%d] ", current+1)
			zero.Do(func(x interface{}) {
				if x == r.Value {
					fmt.Print("(")
					fmt.Print(x)
					fmt.Print(")")
				} else {
					fmt.Print(x)
				}
				fmt.Print(" " )
			})
			fmt.Println("")
		*/
	}
	index := 0
	high := 0
	for i, s := range score {
		if s > high {
			high = s
			index = i
		}
	}
	fmt.Println(high)
	fmt.Println(index)
}
