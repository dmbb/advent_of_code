package main

import (
	"fmt"
	"io/ioutil"
	"strings"
)

var (
	generations = 2000
	offset      = 500
)

type instruction struct {
	l2, l1, c, r1, r2 bool
	r                 bool
}

func main() {
	data, _ := ioutil.ReadFile("2018/Day 12/input.txt")
	input := string(data)

	lines := strings.Split(input, "\n")
	initial := strings.TrimSpace(strings.TrimPrefix(lines[0], "initial state: "))

	plants := make([]bool, 0, 0)
	for i := 0; i < offset; i++ {
		plants = append(plants, false)
	}
	for _, r := range initial {
		if r == '#' {
			plants = append(plants, true)
		} else {
			plants = append(plants, false)
		}
	}
	for i := 0; i < offset; i++ {
		plants = append(plants, false)
	}
	nextGen := make([]bool, len(plants))
	copy(nextGen, plants)

	instructions := make([]instruction, 0)
	for _, line := range lines[2:] {
		var inst instruction
		inst.l2 = line[0] == '#'
		inst.l1 = line[1] == '#'
		inst.c = line[2] == '#'
		inst.r1 = line[3] == '#'
		inst.r2 = line[4] == '#'

		inst.r = line[9] == '#'

		instructions = append(instructions, inst)
	}

	for i := offset; i < len(plants)-offset; i++ {
		if plants[i] {
			fmt.Printf("#")
		} else {
			fmt.Printf(".")
		}
	}
	fmt.Print("\n")
	l2, l1, c, r1, r2 := false, false, false, false, false
	for g := 0; g < generations; g++ {
		for i, plant := range plants {
			if i == 0 {
				l2, l1 = false, false
				c = plant
				r1 = plants[i+1]
				r2 = plants[i+2]
			} else if i == 1 {
				l2 = false
				l1 = plants[i-1]
				c = plant
				r1 = plants[i+1]
				r2 = plants[i+2]
			} else if i == len(plants)-1 {
				l2 = plants[i-2]
				l1 = plants[i-1]
				c = plant
				r1, r2 = false, false
			} else if i == len(plants)-2 {
				l2 = plants[i-2]
				l1 = plants[i-1]
				c = plant
				r1 = plants[i+1]
				r2 = false
			} else {
				l2 = plants[i-2]
				l1 = plants[i-1]
				c = plant
				r1 = plants[i+1]
				r2 = plants[i+2]
			}

			for _, inst := range instructions {
				if inst.l2 == l2 && inst.l1 == l1 && inst.c == c && inst.r1 == r1 && inst.r2 == r2 {
					nextGen[i] = inst.r
					break
				}
				nextGen[i] = false
			}
		}
		same := true
		for i := range plants {
			if i+1 < len(plants) {
				if plants[i] != nextGen[i+1] {
					same = false
				}
			}
		}
		if same {
			fmt.Println(g)
			break
		}
		copy(plants, nextGen)
		for i := offset; i < len(plants)-offset; i++ {
			if plants[i] {
				fmt.Printf("#")
			} else {
				fmt.Printf(".")
			}
		}
		fmt.Print("\n")
	}

	total := 0
	plantNum := 0
	for i := range plants {
		if plants[i] {
			plantNum++
			total += i - offset + 50000000000 - 98
		}
	}
	fmt.Println(plantNum)
	fmt.Println(total)
	fmt.Println(50000000*plantNum + total)
}
