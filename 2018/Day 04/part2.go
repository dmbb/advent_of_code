package main

import (
	"fmt"
	"io/ioutil"
	"sort"
	"strconv"
	"strings"
	"time"
)

type logSlice []log

type log struct {
	ts time.Time
	s  string
}

func (l logSlice) Sort() {
	sort.Sort(l)
}

func (l logSlice) Len() int {
	return len(l)
}

func (l logSlice) Less(i, j int) bool {
	return l[i].ts.Before(l[j].ts)
}

func (l logSlice) Swap(i, j int) {
	l[i], l[j] = l[j], l[i]
}

func main() {
	dat, _ := ioutil.ReadFile("input.txt")
	lines := strings.Split(string(dat), "\n")

	slice := make(logSlice, 0)
	for _, line := range lines {
		var l log

		foo := strings.Split(line[1:], "]")
		l.ts, _ = time.Parse("2006-01-02 15:04", foo[0])
		l.s = foo[1][1:]
		slice = append(slice, l)
	}
	slice.Sort()

	total := make(map[int]int)
	minutes := make(map[int]map[int]int)
	var guard int
	var start time.Time
	for _, l := range slice {
		split := strings.Split(l.s, " ")
		if strings.HasPrefix(l.s, "Guard") {
			guard, _ = strconv.Atoi(split[1][1:])
		} else if strings.HasPrefix(l.s, "falls") {
			start = l.ts
		} else {
			for i := start.Minute(); i < l.ts.Minute(); i++ {
				if minutes[guard] == nil {
					minutes[guard] = make(map[int]int)
				}
				minutes[guard][i]++
				total[guard]++
			}
		}
	}

	topTime := make(map[int]int)
	topMinute := make(map[int]int)
	for g, v := range minutes {
		for min, asleep := range v {
			if asleep > topTime[min] {
				topTime[min] = asleep
				topMinute[min] = g
			}
		}
	}
	fmt.Println(topTime)
	var top int
	var min int
	for k, v := range topTime {
		if v > top {
			top = v
			min = k
		}
	}
	fmt.Println(min * topMinute[min])
}
