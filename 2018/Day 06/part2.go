package main

import (
	"fmt"
	"io/ioutil"
	"math"
	"strconv"
	"strings"
)

type Coordinate struct {
	x, y int
	id   int
}

func main() {
	data, _ := ioutil.ReadFile("2018/Day 06/input.txt")
	input := string(data)

	coords := make([]Coordinate, 0)
	maxX := 0
	maxY := 0
	for i, line := range strings.Split(input, "\n") {
		line = strings.TrimSpace(line)
		strCoords := strings.Split(line, ", ")
		x, _ := strconv.Atoi(strCoords[0])
		if x > maxX {
			maxX = x
		}
		y, _ := strconv.Atoi(strCoords[1])
		if y > maxY {
			maxY = y
		}
		coords = append(coords, Coordinate{x: x + 12500, y: y + 12500, id: i})
	}

	grid := make([][]int, 25000)
	for i := range grid {
		grid[i] = make([]int, 25000)
	}

	count := 0
	for y := range grid {
		for x := range grid[y] {
			totaldist := 0
			for _, coord := range coords {
				xDist := x - coord.x
				yDist := y - coord.y
				xDist = int(math.Abs(float64(xDist)))
				yDist = int(math.Abs(float64(yDist)))
				totaldist += xDist + yDist
			}
			if totaldist < 10000 {
				grid[y][x] = 1
				count++
			} else {
				grid[y][x] = 0
			}
		}
		if y%1000 == 0 {
			fmt.Println(y)
		}
	}
	fmt.Println(count)
	/*
		for y := range grid {
			for x := range grid[y] {
				if grid[y][x] == -1 {
					fmt.Print(".")
				} else {
					fmt.Print(grid[y][x])
				}
			}
			fmt.Print("\n")
		}
	*/

	m := make(map[int]int)
	for y := range grid {
		for x := range grid[y] {
			m[grid[y][x]]++
		}
	}

	delete(m, 0)

	fmt.Println(m)
	maxId := -1
	max := 0
	for k, v := range m {
		if v > max {
			max = v
			maxId = k
		}
	}
	fmt.Printf("%d: %d\n", maxId, max)
}
