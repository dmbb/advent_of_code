package main

import (
	"fmt"
	"io/ioutil"
	"math"
	"strconv"
	"strings"
)

type Coordinate struct {
	x, y int
	id   int
}

func main() {
	data, _ := ioutil.ReadFile("2018/Day 06/input.txt")
	input := string(data)

	coords := make([]Coordinate, 0)
	maxX := 0
	maxY := 0
	for i, line := range strings.Split(input, "\n") {
		line = strings.TrimSpace(line)
		strCoords := strings.Split(line, ", ")
		x, _ := strconv.Atoi(strCoords[0])
		if x > maxX {
			maxX = x
		}
		y, _ := strconv.Atoi(strCoords[1])
		if y > maxY {
			maxY = y
		}
		coords = append(coords, Coordinate{x: x, y: y, id: i})
	}
	fmt.Println(coords)

	grid := make([][]int, maxY+1)
	for i := range grid {
		grid[i] = make([]int, maxX+1)
	}

	for y := range grid {
		for x := range grid[y] {
			closest := make(map[int]struct{})
			closestDist := 100000000
			for _, coord := range coords {
				xDist := coord.x - x
				yDist := coord.y - y
				xDist = int(math.Abs(float64(xDist)))
				yDist = int(math.Abs(float64(yDist)))
				dist := xDist + yDist
				if dist == closestDist {
					closest[coord.id] = struct{}{}
				}
				if dist < closestDist {
					closest = make(map[int]struct{})
					closest[coord.id] = struct{}{}
					closestDist = dist
				}
			}
			if len(closest) == 1 {
				for k := range closest {
					grid[y][x] = k
				}
			} else {
				grid[y][x] = -1
			}
		}
	}
	/*
		for y := range grid {
			for x := range grid[y] {
				if grid[y][x] == -1 {
					fmt.Print(".")
				} else {
					fmt.Print(grid[y][x])
				}
			}
			fmt.Print("\n")
		}
	*/

	m := make(map[int]int)
	for y := range grid {
		for x := range grid[y] {
			m[grid[y][x]]++
		}
	}

	for x := range grid[0] {
		delete(m, grid[0][x])
	}
	for y := range grid {
		delete(m, grid[y][0])
	}
	for y := range grid {
		delete(m, grid[y][maxX])
	}
	for x := range grid[0] {
		delete(m, grid[maxY][x])
	}

	delete(m, -1)

	maxId := -1
	max := 0
	for k, v := range m {
		if v > max {
			max = v
			maxId = k
		}
	}
	fmt.Printf("%d: %d\n", maxId, max)
}
