package main

import "fmt"

var (
	input = 4172
)

func main() {
	bestX := 0
	bestY := 0
	best := 0
	for x := 1; x <= 298; x++ {
		for y := 1; y <= 298; y++ {

			var total int
			for i := 0; i < 3; i++ {
				for j := 0; j < 3; j++ {
					total += calc(x+i, y+j)
				}
			}
			if total > best {
				bestX = x
				bestY = y
				best = total
			}
		}
	}
	fmt.Println(bestX)
	fmt.Println(bestY)
	fmt.Println(best)
}

func calc(x, y int) int {
	var power, id int
	id = x + 10
	power = id
	power = power * y
	power = power + input
	power = power * id
	power = (power / 100) % 10
	power = power - 5
	return power
}
