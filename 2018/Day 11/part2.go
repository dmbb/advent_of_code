package main

import "fmt"

var (
	input = 4172
)

func main() {
	bestX := 0
	bestY := 0
	best := 0
	width := 0
	for x := 1; x <= 300; x++ {
		for y := 1; y <= 300; y++ {

			largest := x
			if y > x {
				largest = y
			}

			for w := 0; w < 300-largest; w++ {
				var total int
				for i := 0; i < w; i++ {
					for j := 0; j < w; j++ {
						total += calc(x+i, y+j)
					}
				}
				if total > best {
					bestX = x
					bestY = y
					best = total
					width = w
				}
			}
		}
		fmt.Println(x)
	}
	fmt.Println(bestX)
	fmt.Println(bestY)
	fmt.Println(width)
	fmt.Println(best)
}

func calc(x, y int) int {
	var power, id int
	id = x + 10
	power = id
	power = power * y
	power = power + input
	power = power * id
	power = (power / 100) % 10
	power = power - 5
	return power
}
