package main

import (
	"fmt"
	"io/ioutil"
	"strings"
	"time"
)

const (
	OPEN = '.'
	TREE = '|'
	LUMB = '#'
	NA   = ' '
)

func main() {
	data, _ := ioutil.ReadFile("2018/Day 18/input.txt")

	grid := make([][]rune, 0)
	newGrid := make([][]rune, 0)
	for y, line := range strings.Split(string(data), "\n") {
		if y == 0 {
			grid = append(grid, make([]rune, 0))
			grid[0] = append(grid[0], NA)
			for range line {
				grid[0] = append(grid[0], NA)
			}
			grid[0] = append(grid[0], NA)
		}
		y = y + 1
		grid = append(grid, make([]rune, 0))
		grid[y] = make([]rune, 0)
		for x, c := range line {
			if x == 0 {
				grid[y] = append(grid[y], NA)
			}
			grid[y] = append(grid[y], c)
			if x+1 == len(line) {
				grid[y] = append(grid[y], NA)
			}
		}
		if y == len(strings.Split(string(data), "\n")) {
			grid = append(grid, make([]rune, 0))
			grid[y+1] = append(grid[y+1], NA)
			for range line {
				grid[y+1] = append(grid[y+1], NA)
			}
			grid[y+1] = append(grid[y+1], NA)
		}
	}

	for y, line := range grid {
		newGrid = append(newGrid, make([]rune, 0))
		for _, c := range line {
			newGrid[y] = append(newGrid[y], NA)
			fmt.Printf("%c", c)
		}
		fmt.Println("")
	}

	for tick := 1; tick <= 10; tick++ {
		time.Sleep(1 * time.Second)
		for y, line := range grid {
			for x := range line {
				newGrid[y][x] = check(y, x, grid)
			}
		}
		for y := range grid {
			copy(grid[y], newGrid[y])
		}

		for _, line := range grid {
			for _, c := range line {
				fmt.Printf("%c", c)
			}
			fmt.Println("")
		}
	}
	lumberTotal := 0
	treeTotal := 0
	for _, line := range grid {
		for _, c := range line {
			if c == LUMB {
				lumberTotal++
			} else if c == TREE {
				treeTotal++
			}
		}
	}
	fmt.Printf("lumb: %d, tree: %d, total: %d", lumberTotal, treeTotal, lumberTotal*treeTotal)
}

func check(y, x int, grid [][]rune) rune {
	switch grid[y][x] {
	case OPEN:
		trees := 0
		for i := -1; i <= 1; i++ {
			for j := -1; j <= 1; j++ {
				if j == 0 && i == 0 {
					continue
				}
				if grid[y+i][x+j] == TREE {
					trees++
				}
			}
		}
		if trees >= 3 {
			return TREE
		} else {
			return OPEN
		}
	case TREE:
		lumber := 0
		for i := -1; i <= 1; i++ {
			for j := -1; j <= 1; j++ {
				if j == 0 && i == 0 {
					continue
				}
				if grid[y+i][x+j] == LUMB {
					lumber++
				}
			}
		}
		if lumber >= 3 {
			return LUMB
		} else {
			return TREE
		}
	case LUMB:
		var tree, lumber bool
		for i := -1; i <= 1; i++ {
			for j := -1; j <= 1; j++ {
				if j == 0 && i == 0 {
					continue
				}
				if grid[y+i][x+j] == LUMB {
					lumber = true
				} else if grid[y+i][x+j] == TREE {
					tree = true
				}
			}
		}
		if tree && lumber {
			return LUMB
		} else {
			return OPEN
		}
	case NA:
		return NA
	}
	return NA
}
