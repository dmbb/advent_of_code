package main

// CHECK LUMBER OUTPUTS FOR CYCLE

import (
	"fmt"
	"io/ioutil"
	"strings"
	"time"
)

const (
	OPEN = '.'
	TREE = '|'
	LUMB = '#'
	NA   = ' '
)

func main() {
	data, _ := ioutil.ReadFile("2018/Day 18/input.txt")

	grid := make([][]rune, 0)
	newGrid := make([][]rune, 0)
	grid200 := make([][]rune, 0)
	for y, line := range strings.Split(string(data), "\n") {
		if y == 0 {
			grid = append(grid, make([]rune, 0))
			grid[0] = append(grid[0], NA)
			for range line {
				grid[0] = append(grid[0], NA)
			}
			grid[0] = append(grid[0], NA)
		}
		y = y + 1
		grid = append(grid, make([]rune, 0))
		grid[y] = make([]rune, 0)
		for x, c := range line {
			if x == 0 {
				grid[y] = append(grid[y], NA)
			}
			grid[y] = append(grid[y], c)
			if x+1 == len(line) {
				grid[y] = append(grid[y], NA)
			}
		}
		if y == len(strings.Split(string(data), "\n")) {
			grid = append(grid, make([]rune, 0))
			grid[y+1] = append(grid[y+1], NA)
			for range line {
				grid[y+1] = append(grid[y+1], NA)
			}
			grid[y+1] = append(grid[y+1], NA)
		}
	}

	for y, line := range grid {
		newGrid = append(newGrid, make([]rune, 0))
		grid200 = append(grid200, make([]rune, 0))
		for _, c := range line {
			newGrid[y] = append(newGrid[y], NA)
			grid200[y] = append(grid200[y], NA)
			fmt.Printf("%c", c)
		}
		fmt.Println("")
	}

	fmt.Println((1000000000 - 1000) % 28)
	time.Sleep(1 * time.Minute)
	/*
				942 ---- lumb: 367, tree: 584, total: 214328
		943 ---- lumb: 368, tree: 573, total: 210864
		944 ---- lumb: 380, tree: 558, total: 212040
		945 ---- lumb: 368, tree: 551, total: 202768
		946 ---- lumb: 359, tree: 546, total: 196014
		947 ---- lumb: 342, tree: 549, total: 187758
		948 ---- lumb: 343, tree: 546, total: 187278
		949 ---- lumb: 318, tree: 548, total: 174264
		950 ---- lumb: 324, tree: 544, total: 176256
		951 ---- lumb: 323, tree: 542, total: 175066
		952 ---- lumb: 329, tree: 537, total: 176673
		953 ---- lumb: 327, tree: 535, total: 174945
		954 ---- lumb: 335, tree: 528, total: 176880
		955 ---- lumb: 326, tree: 527, total: 171802
		956 ---- lumb: 325, tree: 525, total: 170625
		957 ---- lumb: 320, tree: 528, total: 168960
		958 ---- lumb: 325, tree: 530, total: 172250
		959 ---- lumb: 319, tree: 536, total: 170984
		960 ---- lumb: 324, tree: 539, total: 174636
		961 ---- lumb: 322, tree: 547, total: 176134
		962 ---- lumb: 325, tree: 553, total: 179725
		963 ---- lumb: 319, tree: 563, total: 179597
		964 ---- lumb: 327, tree: 569, total: 186063
		965 ---- lumb: 330, tree: 579, total: 191070
		966 ---- lumb: 339, tree: 588, total: 199332
		967 ---- lumb: 344, tree: 594, total: 204336
		968 ---- lumb: 356, tree: 591, total: 210396
		969 ---- lumb: 357, tree: 592, total: 211344
		970 ---- lumb: 367, tree: 584, total: 214328
		971 ---- lumb: 368, tree: 573, total: 210864
		972 ---- lumb: 380, tree: 558, total: 212040
		973 ---- lumb: 368, tree: 551, total: 202768
		974 ---- lumb: 359, tree: 546, total: 196014
		975 ---- lumb: 342, tree: 549, total: 187758
		976 ---- lumb: 343, tree: 546, total: 187278
		977 ---- lumb: 318, tree: 548, total: 174264
		978 ---- lumb: 324, tree: 544, total: 176256
		979 ---- lumb: 323, tree: 542, total: 175066
		980 ---- lumb: 329, tree: 537, total: 176673
		981 ---- lumb: 327, tree: 535, total: 174945
		982 ---- lumb: 335, tree: 528, total: 176880
		983 ---- lumb: 326, tree: 527, total: 171802
		984 ---- lumb: 325, tree: 525, total: 170625
		985 ---- lumb: 320, tree: 528, total: 168960
		986 ---- lumb: 325, tree: 530, total: 172250
		987 ---- lumb: 319, tree: 536, total: 170984
		988 ---- lumb: 324, tree: 539, total: 174636
		989 ---- lumb: 322, tree: 547, total: 176134
		990 ---- lumb: 325, tree: 553, total: 179725
		991 ---- lumb: 319, tree: 563, total: 179597
		992 ---- lumb: 327, tree: 569, total: 186063
		993 ---- lumb: 330, tree: 579, total: 191070
		994 ---- lumb: 339, tree: 588, total: 199332
		995 ---- lumb: 344, tree: 594, total: 204336
		996 ---- lumb: 356, tree: 591, total: 210396
		997 ---- lumb: 357, tree: 592, total: 211344
		998 ---- lumb: 367, tree: 584, total: 214328
	*/

	for tick := 1; ; tick++ {
		time.Sleep(100 * time.Millisecond)
		for y, line := range grid {
			for x := range line {
				newGrid[y][x] = check(y, x, grid)
			}
		}
		for y := range grid {
			copy(grid[y], newGrid[y])
		}

		if tick == 100 {
			for y := range grid {
				copy(grid200[y], grid[y])
			}
		}
		if tick > 100 {
			var equal bool
			for y, line := range grid {
				for x, c := range line {
					if c != grid200[y][x] {
						equal = false
					}
				}
			}

			if equal {
				fmt.Println(tick)
				fmt.Println(tick - 200)
				return
			}

		}
		lumberTotal := 0
		treeTotal := 0
		for _, line := range grid {
			for _, c := range line {
				if c == LUMB {
					lumberTotal++
				} else if c == TREE {
					treeTotal++
				}
			}
		}
		fmt.Printf("%d ---- lumb: %d, tree: %d, total: %d\n", tick, lumberTotal, treeTotal, lumberTotal*treeTotal)
		/*
			for y, line := range grid {
				for _, c := range line {
					fmt.Printf("%c", c)
				}
				if tick > 200 {
					for _, c := range grid200[y] {
						fmt.Printf("%c", c)
					}
				}
				fmt.Println("")
			}
			fmt.Printf("\t%d\n", tick)
		*/
	}
}

func check(y, x int, grid [][]rune) rune {
	switch grid[y][x] {
	case OPEN:
		trees := 0
		for i := -1; i <= 1; i++ {
			for j := -1; j <= 1; j++ {
				if j == 0 && i == 0 {
					continue
				}
				if grid[y+i][x+j] == TREE {
					trees++
				}
			}
		}
		if trees >= 3 {
			return TREE
		} else {
			return OPEN
		}
	case TREE:
		lumber := 0
		for i := -1; i <= 1; i++ {
			for j := -1; j <= 1; j++ {
				if j == 0 && i == 0 {
					continue
				}
				if grid[y+i][x+j] == LUMB {
					lumber++
				}
			}
		}
		if lumber >= 3 {
			return LUMB
		} else {
			return TREE
		}
	case LUMB:
		var tree, lumber bool
		for i := -1; i <= 1; i++ {
			for j := -1; j <= 1; j++ {
				if j == 0 && i == 0 {
					continue
				}
				if grid[y+i][x+j] == LUMB {
					lumber = true
				} else if grid[y+i][x+j] == TREE {
					tree = true
				}
			}
		}
		if tree && lumber {
			return LUMB
		} else {
			return OPEN
		}
	case NA:
		return NA
	}
	return NA
}
