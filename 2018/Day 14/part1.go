package main

import (
	"container/ring"
	"fmt"
)

var (
	test  = 2018
	input = 165061
)

func main() {
	r := ring.New(2)
	r.Value = 3
	r.Next().Value = 7

	f, s := r, r
	s = r.Next()

	match := []int{0, 1, 2, 4, 5}
	//match := []int{1,6,5,0,6,1}
	i := 0
	for j := 0; j < 20; j++ {
		combine := f.Value.(int) + s.Value.(int)
		end := r.Prev()
		new := ring.New(1)
		if combine >= 10 {
			new.Value = combine / 10
			i += 1
			new.Link(ring.New(1))
			new.Next().Value = combine % 10
			i += 1
		} else {
			new.Value = combine % 10
			i += 1
		}
		end.Link(new)

		f = f.Move(f.Value.(int) + 1)
		s = s.Move(s.Value.(int) + 1)

		index := -1
		matchIndex := 0
		m := false
		r.Do(func(x interface{}) {
			if m == true {
				return
			}
			index++
			if x.(int) == match[matchIndex] {
				matchIndex++
				fmt.Print(x)
			} else {
				if matchIndex != 0 {
					fmt.Println("")
				}
				matchIndex = 0
			}
			if matchIndex == len(match) {
				m = true
			}
		})
		if m {
			fmt.Println(i)
			fmt.Println(index - len(match))
			return
		}
	}
}
