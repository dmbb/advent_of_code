package main

import (
	"container/ring"
	"fmt"
)

var (
	//input = []int{5,9,4,1,4}
	//input = []int{9,2,5,1,0}
	//input = []int{0,1,2,4,5}
	input = []int{1, 6, 5, 0, 6, 1}
)

func main() {
	r := ring.New(2)
	r.Value = 3
	r.Next().Value = 7

	f, s := r, r
	s = r.Next()

	i := 2
	index := 0
	for {
		combine := f.Value.(int) + s.Value.(int)
		end := r.Prev()
		new := ring.New(1)
		if combine >= 10 {
			new.Value = combine / 10
			i += 1
			if combine/10 == input[index] {
				index++
				if index == len(input) {
					fmt.Println(i - len(input))
					return
				}
			} else {
				index = 0
				if combine/10 == input[index] {
					index++
				}
			}
			new.Link(ring.New(1))
			new.Next().Value = combine % 10
			i += 1
			if combine%10 == input[index] {
				index++
				if index == len(input) {
					fmt.Println(i - len(input))
					return
				}
			} else {
				index = 0
				if combine%10 == input[index] {
					index++
				}
			}
		} else {
			new.Value = combine % 10
			i += 1
			if combine%10 == input[index] {
				index++
				if index == len(input) {
					fmt.Println(i)
					return
				}
			} else {
				index = 0
				if combine%10 == input[index] {
					index++
				}
			}
		}
		end.Link(new)

		f = f.Move(f.Value.(int) + 1)
		s = s.Move(s.Value.(int) + 1)

	}
}
