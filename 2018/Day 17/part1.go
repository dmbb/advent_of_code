package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

type point struct {
	x, y int
	dead bool
}

func main() {
	grid := make([][]rune, 2000)
	for y, line := range grid {
		grid[y] = make([]rune, 1000)
		for x := range line {
			grid[y][x] = '.'
		}
	}
	for y := range grid {
		for x := range grid[y] {
			grid[y][x] = '.'
		}
	}

	data, _ := ioutil.ReadFile("2018/Day 17/input.txt")

	maxY := 0
	minY := 100000000000000
	for _, line := range strings.Split(string(data), "\n") {
		inputs := strings.Split(line, ", ")

		if inputs[0][0] == 'x' {
			x, _ := strconv.Atoi(inputs[0][2:])
			set := strings.Split(inputs[1][2:], "..")
			first, _ := strconv.Atoi(set[0])
			second, _ := strconv.Atoi(set[1])
			for y := first; y <= second; y++ {
				grid[y][x] = '#'
				if y > maxY {
					maxY = y
				}
				if y < minY {
					minY = y
				}
			}
		} else {
			y, _ := strconv.Atoi(inputs[0][2:])
			if y > maxY {
				maxY = y
			}
			if y < minY {
				minY = y
			}
			set := strings.Split(inputs[1][2:], "..")
			first, _ := strconv.Atoi(set[0])
			second, _ := strconv.Atoi(set[1])
			for x := first; x <= second; x++ {
				grid[y][x] = '#'
			}
		}
	}

	grid[0][500] = '+'

	water := point{500, 0, false}
	waterSource := make([]point, 0)
	waterSource = append(waterSource, water)

	// Go downwards.
	// If you can't go downwards, go to each side.
	// If you can't go to each side, go up once

	for {
		for i, w := range waterSource {
			if w.y >= maxY {
				waterSource[i].dead = true
				continue
			}
			allDead := true
			for _, s := range waterSource {
				if s.dead == false {
					allDead = false
					break
				}
			}
			if allDead {
				for y := 0; y <= maxY+1; y++ {
					for x := 380; x < 620; x++ {
						fmt.Printf("%c", grid[y][x])
					}
					fmt.Printf(" %d\n", y)
				}

				tildes := 0
				total := 0
				for y, g := range grid {
					if y < minY {
						continue
					}
					if y > maxY {
						continue
					}
					for _, c := range g {
						if c == '~' {
							tildes++
							total++
						}
						if c == '|' {
							total++
						}
					}
				}
				fmt.Printf("part 2 (tildes): %d, part 1 (all): %d", tildes, total)
				return
			}
			if w.dead {
				continue
			}
			var below point
			below.x = w.x
			below.y = w.y + 1

			switch grid[below.y][below.x] {
			case '|':
				waterSource[i].dead = true
			case '.': // fall down
				waterSource[i].y = below.y
				waterSource[i].x = below.x
				grid[below.y][below.x] = '|'
			case '~':
				fallthrough
			case '#': // break off left and right
				// left
				var end bool
				var nextL, nextR point
				nextL.x = w.x
				nextL.y = w.y
				nextR.x = w.x
				nextR.y = w.y
				for {
					if nextL.dead && nextR.dead {
						if end {
							if grid[nextL.y+1][nextL.x] == '.' {
								waterSource = append(waterSource, point{y: nextL.y, x: nextL.x, dead: false})
							}
							if grid[nextR.y+1][nextR.x] == '.' {
								waterSource = append(waterSource, point{y: nextR.y, x: nextR.x, dead: false})
							}
							for j := nextL.x + 1; j < nextR.x; j++ {
								grid[nextL.y][j] = '|'
							}

							break
						}
						nextL.x = w.x
						nextR.x = w.x
						nextL.y = nextL.y - 1
						nextR.y = nextR.y - 1
						nextL.dead = false
						nextR.dead = false
					}

					if grid[nextL.y][nextL.x] == '#' {
						for j := nextL.x + 1; j <= w.x; j++ {
							grid[nextL.y][j] = '~'
						}
						nextL.dead = true
					}
					if grid[nextR.y][nextR.x] == '#' {
						for j := nextR.x - 1; j >= w.x; j-- {
							grid[nextR.y][j] = '~'
						}
						nextR.dead = true
					}

					if !nextL.dead {
						if grid[nextL.y+1][nextL.x] == '.' || (grid[nextL.y][nextL.x] == '|' && grid[nextL.y+1][nextL.x] == '|') {
							nextL.dead = true
							end = true
							grid[nextL.y][nextL.x] = '|'
						} else {
							if grid[nextL.y+1][nextL.x] == '|' {
								grid[nextL.y+1][nextL.x] = '~'
							}
							grid[nextL.y][nextL.x] = '|'
							nextL.x = nextL.x - 1
						}
					}

					if !nextR.dead {
						grid[nextR.y][nextR.x] = '|'
						if grid[nextR.y+1][nextR.x] == '.' {
							nextR.dead = true
							end = true
						} else {
							if grid[nextR.y+1][nextR.x] == '|' {
								grid[nextR.y+1][nextR.x] = '~'
							}

							nextR.x = nextR.x + 1
						}
					}
				}
				waterSource[i].dead = true
			}
		}
	}
}
