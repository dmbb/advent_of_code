package main

import (
	"fmt"
	"time"
)

/* This program is finding the sum of all the factors.

(1, 2, 4, 67, 134, 268, 39371, 78742, 157484, 2637857, 5275714, 10551428)
18741072 is the sum.
*/
func main() {
	r0, r1, r2, r3, r4 := 0, 0, 0, 0, 0
	r0 = 1

	r2 = r2 + 2
	r2 = r2 * r2
	r2 = r2 * 19
	r2 = r2 * 11
	r3 = ((r3 + 8) * 22) + 16
	r2 = r2 + r3

	if r0 == 1 {
		r3 = ((((27 * 28) + 29) * 30) * 14) * 32
		fmt.Println(r3)
		r2 = r2 + r3
		fmt.Println(r2)
	}
	fmt.Printf("[%d %d %d %d %d]\n", r0, r1, r2, r3, r4)
	time.Sleep(1 * time.Second)

	r1 = 1
second:
	r4 = 1
first:
	r3 = r1 * r4
	if r3 == r2 {
		r0 = r1 + r0
		fmt.Printf("[%d %d %d %d %d]\n", r0, r1, r2, r3, r4)
		r3 = 1
	}
	r3 = 0
	r4++
	if r4 <= r2 {
		r3 = 1
		goto first
	}
	r3 = 0
	r1++
	if r1 <= r2 {
		r3 = 1
		goto second
	}
	r3 = 0
	fmt.Printf("[%d %d %d %d %d]\n", r0, r1, r2, r3, r4)
	//[1806 1029 1028 1 1029 257]
}
