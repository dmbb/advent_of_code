package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
	"time"
)

func main() {
	var ip int

	ip = 5

	data, _ := ioutil.ReadFile("2018/Day 19/input.txt")
	program := strings.Split(string(data), "\n")
	program = append(program[1:])

	registers := make([]uint32, 6, 6)

	for registers[ip] < uint32(len(program)) {
		line := strings.Split(program[registers[ip]], " ")
		fmt.Printf("ip=%d %v %v\n", registers[ip], registers, line)
		time.Sleep(100 * time.Millisecond)

		op := make([]uint32, 4, 4)
		op1, _ := strconv.Atoi(line[1])
		op2, _ := strconv.Atoi(line[2])
		op3, _ := strconv.Atoi(line[3])
		op[1] = uint32(op1)
		op[2] = uint32(op2)
		op[3] = uint32(op3)

		switch line[0] {
		case "seti":
			registers = seti(registers, op)
		case "setr":
			registers = setr(registers, op)
		case "addi":
			registers = addi(registers, op)
		case "addr":
			registers = addr(registers, op)
		case "muli":
			registers = muli(registers, op)
		case "mulr":
			registers = mulr(registers, op)
		case "bani":
			registers = bani(registers, op)
		case "banr":
			registers = banr(registers, op)
		case "bori":
			registers = bori(registers, op)
		case "borr":
			registers = bani(registers, op)
		case "gtir":
			registers = gtir(registers, op)
		case "gtrr":
			registers = gtrr(registers, op)
		case "gtri":
			registers = gtri(registers, op)
		case "eqir":
			registers = eqir(registers, op)
		case "eqrr":
			registers = eqrr(registers, op)
		case "eqri":
			registers = eqri(registers, op)
		default:
			fmt.Println(line)
			return
		}
		registers[ip]++
	}
	fmt.Println(registers)
}

func addr(before, op []uint32) []uint32 {
	after := make([]uint32, 6)
	copy(after, before)

	after[op[3]] = before[op[1]] + before[op[2]]
	return after
}

func mulr(before, op []uint32) []uint32 {
	after := make([]uint32, 6)
	copy(after, before)

	after[op[3]] = before[op[1]] * before[op[2]]
	return after
}

func banr(before, op []uint32) []uint32 {
	after := make([]uint32, 6)
	copy(after, before)

	after[op[3]] = before[op[1]] & before[op[2]]
	return after
}

func borr(before, op []uint32) []uint32 {
	after := make([]uint32, 6)
	copy(after, before)

	after[op[3]] = before[op[1]] | before[op[2]]
	return after
}

func setr(before, op []uint32) []uint32 {
	after := make([]uint32, 6)
	copy(after, before)

	after[op[3]] = before[op[1]]
	return after
}

func addi(before, op []uint32) []uint32 {
	after := make([]uint32, 6)
	copy(after, before)

	after[op[3]] = before[op[1]] + op[2]
	return after
}

func muli(before, op []uint32) []uint32 {
	after := make([]uint32, 6)
	copy(after, before)

	after[op[3]] = before[op[1]] * op[2]
	return after
}

func bani(before, op []uint32) []uint32 {
	after := make([]uint32, 6)
	copy(after, before)

	after[op[3]] = before[op[1]] & op[2]
	return after
}

func bori(before, op []uint32) []uint32 {
	after := make([]uint32, 6)
	copy(after, before)

	after[op[3]] = before[op[1]] | op[2]
	return after
}

func seti(before, op []uint32) []uint32 {
	after := make([]uint32, 6)
	copy(after, before)

	after[op[3]] = op[1]
	return after
}

func gtir(before, op []uint32) []uint32 {
	after := make([]uint32, 6)
	copy(after, before)
	if op[1] > after[op[2]] {
		after[op[3]] = 1
	} else {
		after[op[3]] = 0
	}

	return after
}

func gtri(before, op []uint32) []uint32 {
	after := make([]uint32, 6)
	copy(after, before)
	if before[op[1]] > op[2] {
		after[op[3]] = 1
	} else {
		after[op[3]] = 0
	}

	return after
}

func gtrr(before, op []uint32) []uint32 {
	after := make([]uint32, 6)
	copy(after, before)
	if before[op[1]] > after[op[2]] {
		after[op[3]] = 1
	} else {
		after[op[3]] = 0
	}

	return after
}

func eqir(before, op []uint32) []uint32 {
	after := make([]uint32, 6)
	copy(after, before)
	if op[1] == after[op[2]] {
		after[op[3]] = 1
	} else {
		after[op[3]] = 0
	}

	return after
}

func eqri(before, op []uint32) []uint32 {
	after := make([]uint32, 6)
	copy(after, before)
	if before[op[1]] == op[2] {
		after[op[3]] = 1
	} else {
		after[op[3]] = 0
	}

	return after
}

func eqrr(before, op []uint32) []uint32 {
	after := make([]uint32, 6)
	copy(after, before)
	if before[op[1]] == after[op[2]] {
		after[op[3]] = 1
	} else {
		after[op[3]] = 0
	}

	return after
}
