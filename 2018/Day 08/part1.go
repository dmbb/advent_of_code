package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

func main() {

	data, _ := ioutil.ReadFile("2018/Day 08/input.txt")
	ints := make([]int, 0)

	for _, number := range strings.Split(string(data), " ") {
		n, _ := strconv.Atoi(number)
		ints = append(ints, n)
	}

	fmt.Println(recurse(ints, 0))

}

func recurse(input []int, index int) (int, int) {
	total := 0
	children := input[index]
	index++
	entries := input[index]
	index++
	m := make(map[int]int)
	for i := 0; i < children; i++ {
		var sum int
		index, sum = recurse(input, index)
		m[i+1] = sum
	}
	fmt.Println(m)
	if children > 0 {
		for i := 0; i < entries; i++ {
			total += m[input[index]]
			fmt.Println(m[input[index]])
			index++
		}
	} else {
		for i := 0; i < entries; i++ {
			total += input[index]
			index++
		}
	}
	return index, total
}
