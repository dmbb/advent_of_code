package main

import (
	"fmt"
	"io/ioutil"
	"sort"
	"strings"
)

type order struct {
	before, after string
}
type letterSet []*letter

type letter struct {
	s      string
	before int
	done   bool
	n      letterSet
	b      letterSet
}

func (l letterSet) Sort() {
	sort.Sort(l)
}

func (l letterSet) Len() int {
	return len(l)
}

func (l letterSet) Less(i, j int) bool {
	return l[i].s < l[j].s
}

func (l letterSet) Swap(i, j int) {
	l[i], l[j] = l[j], l[i]
}

type worker struct {
	l *letter
	t int
}

func main() {
	data, _ := ioutil.ReadFile("2018/Day 07/input.txt")
	input := string(data)

	orderSlice := make([]order, 0)
	m := make(map[string]*letter)
	for _, line := range strings.Split(input, "\n") {
		split := strings.Split(line, " ")
		m[split[1]] = &letter{s: split[1]}
		m[split[7]] = &letter{s: split[7]}
		orderSlice = append(orderSlice, order{before: split[1], after: split[7]})
	}

	for _, o := range orderSlice {
		m[o.before].n = append(m[o.before].n, m[o.after])
		m[o.after].b = append(m[o.after].b, m[o.before])
		m[o.after].before++
	}

	zero := letter{s: "~"}
	for _, v := range m {
		if v.before == 0 {
			zero.n = append(zero.n, v)
			v.b = append(v.b, &zero)
		}
	}
	zero.done = true

	next := make(letterSet, 0)
	next = append(next, zero.n...)
	workers := make([]*worker, 5)
	for i := range workers {
		workers[i] = &worker{}
	}

	for tick := 0; ; tick++ {
		next.Sort()
		for _, w := range workers {
			if w.l == nil {
				for i := range next {
					//fmt.Printf("worker #%d: assigned letter %s\n", n, next[i].s)
					done := true
					for _, p := range next[i].b {
						if p.done != true {
							done = false
						}
					}
					if !done {
						continue
					}
					w.l = next[i]
					w.t = int(next[i].s[0]) - 4
					next = append(next[:i], next[i+1:]...)
					break
				}
			}
		}

		for _, w := range workers {
			if w.l == nil {
				continue
			}
			//fmt.Printf("worker #%d: time left %d\n", n, w.t)
			if w.t != 0 {
				w.t--
			}
			if w.t == 0 {
				if w.l.s == "J" {
					fmt.Println(tick + 1)
					return
				}
				//fmt.Printf("worker #%d: done letter %s\n", n, w.l.s)
				fmt.Print(w.l.s)
				w.l.done = true
				w.l.n.Sort()
				for _, new := range w.l.n {
					contains := false
					for _, c := range next {
						if c == new {
							contains = true
						}
					}
					if !contains {
						next = append(next, new)
					}
				}
				w.l = nil
			}
		}
	}
}
