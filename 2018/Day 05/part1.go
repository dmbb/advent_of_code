package main

import (
	"fmt"
	"io/ioutil"
	"strings"
	"unicode"
)

func main() {
	dat, _ := ioutil.ReadFile("2018/Day 05/input.txt")
	input := strings.TrimSpace(string(dat))

Exit:
	for {
		for i := 0; i < len(input)-1; i++ {
			first := rune(input[i])
			second := rune(input[i+1])

			if unicode.ToUpper(first) == second && unicode.ToLower(second) == first || unicode.ToLower(first) == second && unicode.ToUpper(second) == first {
				input = input[:i] + input[i+2:]
				continue Exit
			}
		}
		break

	}
	fmt.Println(len(input))

}
