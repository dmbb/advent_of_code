package main

import (
	"fmt"
	"io/ioutil"
	"strings"
	"unicode"
)

func main() {
	dat, _ := ioutil.ReadFile("2018/Day 05/input.txt")
	raw := strings.TrimSpace(string(dat))

	alphabet := "abcdefghijklmnopqrstuvwxyz"
	for _, letter := range alphabet {
		input := raw
		input = strings.Replace(input, string(letter), "", -1)
		input = strings.Replace(input, string(unicode.ToUpper(rune(letter))), "", -1)
	Exit:
		for {
			for i := 0; i < len(input)-1; i++ {
				first := rune(input[i])
				second := rune(input[i+1])

				if unicode.ToUpper(first) == second && unicode.ToLower(second) == first || unicode.ToLower(first) == second && unicode.ToUpper(second) == first {
					input = input[:i] + input[i+2:]
					continue Exit
				}
			}
			break

		}
		fmt.Printf("%s : %d\n", string(letter), len(input))
	}

}
