package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

type test struct {
	before, op, after []uint32
}

func main() {
	instructions := make([]func(before, op []uint32) ([]uint32, bool), 16)
	instructions[0] = addr
	instructions[1] = mulr
	instructions[2] = banr
	instructions[3] = borr
	instructions[4] = setr
	instructions[5] = addi
	instructions[6] = muli
	instructions[7] = bani
	instructions[8] = bori
	instructions[9] = seti
	instructions[10] = gtir
	instructions[11] = gtri
	instructions[12] = gtrr
	instructions[13] = eqir
	instructions[14] = eqri
	instructions[15] = eqrr

	data, _ := ioutil.ReadFile("2018/Day 16/input.txt")

	var tests []test
	for _, slice := range strings.Split(string(data), "\n\n") {
		var t test
		content := strings.Split(slice, "\n")
		content[0] = strings.TrimPrefix(content[0], "Before: [")
		content[2] = strings.TrimPrefix(content[2], "After:  [")
		content[0] = strings.TrimSuffix(content[0], "]")
		content[2] = strings.TrimSuffix(content[2], "]")

		for _, s := range strings.Split(content[0], ", ") {
			n, _ := strconv.Atoi(s)
			x := uint32(n)
			t.before = append(t.before, x)
		}
		for _, s := range strings.Split(content[1], " ") {
			n, _ := strconv.Atoi(s)
			x := uint32(n)
			t.op = append(t.op, x)
		}
		for _, s := range strings.Split(content[2], ", ") {
			n, _ := strconv.Atoi(s)
			x := uint32(n)
			t.after = append(t.after, x)
		}
		tests = append(tests, t)
	}

	valids := make(map[uint32][]int)
	m := make(map[uint32]int)
	total := 0
	for j := 0; j < 3; j++ {
		for _, t := range tests {
			if _, ok := m[t.op[0]]; ok {
				continue
			}
			validTests := 0
			validInstr := make([]int, 0)
			for i, instr := range instructions {
				after, valid := instr(t.before, t.op)
				if !valid {
					continue
				}
				if after[0] == t.after[0] && after[1] == t.after[1] && after[2] == t.after[2] && after[3] == t.after[3] {
					in := false
					for _, v := range m {
						if v == i {
							in = true
						}
					}
					if in {
						continue
					}
					validTests++
					validInstr = append(validInstr, i)
				}
			}
			if validTests == 1 {
				m[t.op[0]] = validInstr[0]
			}
			if validTests >= 3 {
				total++
			}
			valids[t.op[0]] = validInstr
		}
	}
	fmt.Println(m)
	for i := 1; i < 16; i++ {
		fmt.Printf("code %d: ", i)
		for _, n := range valids[uint32(i)] {
			fmt.Printf("\t%d\t", n)
		}
		fmt.Println("")
	}

	data, _ = ioutil.ReadFile("2018/Day 16/input2.txt")
	registers := make([]uint32, 4)
	registers[0] = 0
	registers[1] = 0
	registers[2] = 0
	registers[3] = 0
	for _, line := range strings.Split(string(data), "\n") {
		opcodes := make([]uint32, 0)
		for _, s := range strings.Split(line, " ") {
			n, _ := strconv.Atoi(s)
			x := uint32(n)
			opcodes = append(opcodes, x)
		}
		registers, _ = instructions[m[opcodes[0]]](registers, opcodes)
	}
	fmt.Println(registers)

}

func addr(before, op []uint32) ([]uint32, bool) {
	if op[3] < 0 || op[3] > 3 {
		return nil, false
	}
	if op[1] < 0 || op[1] > 3 {
		return nil, false
	}
	if op[2] < 0 || op[2] > 3 {
		return nil, false
	}
	after := make([]uint32, 4)
	copy(after, before)

	after[op[3]] = before[op[1]] + before[op[2]]
	return after, true
}

func mulr(before, op []uint32) ([]uint32, bool) {
	if op[3] < 0 || op[3] > 3 {
		return nil, false
	}
	if op[1] < 0 || op[1] > 3 {
		return nil, false
	}
	if op[2] < 0 || op[2] > 3 {
		return nil, false
	}
	after := make([]uint32, 4)
	copy(after, before)

	after[op[3]] = before[op[1]] * before[op[2]]
	return after, true
}

func banr(before, op []uint32) ([]uint32, bool) {
	if op[3] < 0 || op[3] > 3 {
		return nil, false
	}
	if op[1] < 0 || op[1] > 3 {
		return nil, false
	}
	if op[2] < 0 || op[2] > 3 {
		return nil, false
	}
	after := make([]uint32, 4)
	copy(after, before)

	after[op[3]] = before[op[1]] & before[op[2]]
	return after, true
}

func borr(before, op []uint32) ([]uint32, bool) {
	if op[3] < 0 || op[3] > 3 {
		return nil, false
	}
	if op[1] < 0 || op[1] > 3 {
		return nil, false
	}
	if op[2] < 0 || op[2] > 3 {
		return nil, false
	}
	after := make([]uint32, 4)
	copy(after, before)

	after[op[3]] = before[op[1]] | before[op[2]]
	return after, true
}

func setr(before, op []uint32) ([]uint32, bool) {
	if op[3] < 0 || op[3] > 3 {
		return nil, false
	}
	if op[1] < 0 || op[1] > 3 {
		return nil, false
	}
	if op[2] < 0 || op[2] > 3 {
		return nil, false
	}
	after := make([]uint32, 4)
	copy(after, before)

	after[op[3]] = before[op[1]]
	return after, true
}

func addi(before, op []uint32) ([]uint32, bool) {
	if op[3] < 0 || op[3] > 3 {
		return nil, false
	}
	if op[1] < 0 || op[1] > 3 {
		return nil, false
	}
	after := make([]uint32, 4)
	copy(after, before)

	after[op[3]] = before[op[1]] + op[2]
	return after, true
}

func muli(before, op []uint32) ([]uint32, bool) {
	if op[3] < 0 || op[3] > 3 {
		return nil, false
	}
	if op[1] < 0 || op[1] > 3 {
		return nil, false
	}
	after := make([]uint32, 4)
	copy(after, before)

	after[op[3]] = before[op[1]] * op[2]
	return after, true
}

func bani(before, op []uint32) ([]uint32, bool) {
	if op[3] < 0 || op[3] > 3 {
		return nil, false
	}
	if op[1] < 0 || op[1] > 3 {
		return nil, false
	}
	after := make([]uint32, 4)
	copy(after, before)

	after[op[3]] = before[op[1]] & op[2]
	return after, true
}

func bori(before, op []uint32) ([]uint32, bool) {
	if op[3] < 0 || op[3] > 3 {
		return nil, false
	}
	if op[1] < 0 || op[1] > 3 {
		return nil, false
	}
	after := make([]uint32, 4)
	copy(after, before)

	after[op[3]] = before[op[1]] | op[2]
	return after, true
}

func seti(before, op []uint32) ([]uint32, bool) {
	if op[3] < 0 || op[3] > 3 {
		return nil, false
	}
	if op[1] < 0 || op[1] > 3 {
		return nil, false
	}
	after := make([]uint32, 4)
	copy(after, before)

	after[op[3]] = op[1]
	return after, true
}

func gtir(before, op []uint32) ([]uint32, bool) {
	if op[3] < 0 || op[3] > 3 {
		return nil, false
	}
	if op[2] < 0 || op[2] > 3 {
		return nil, false
	}
	after := make([]uint32, 4)
	copy(after, before)
	if op[1] > after[op[2]] {
		after[op[3]] = 1
	} else {
		after[op[3]] = 0
	}

	return after, true
}

func gtri(before, op []uint32) ([]uint32, bool) {
	if op[3] < 0 || op[3] > 3 {
		return nil, false
	}
	if op[1] < 0 || op[1] > 3 {
		return nil, false
	}
	after := make([]uint32, 4)
	copy(after, before)
	if before[op[1]] > op[2] {
		after[op[3]] = 1
	} else {
		after[op[3]] = 0
	}

	return after, true
}

func gtrr(before, op []uint32) ([]uint32, bool) {
	if op[3] < 0 || op[3] > 3 {
		return nil, false
	}
	if op[1] < 0 || op[1] > 3 {
		return nil, false
	}
	if op[2] < 0 || op[2] > 3 {
		return nil, false
	}
	after := make([]uint32, 4)
	copy(after, before)
	if before[op[1]] > after[op[2]] {
		after[op[3]] = 1
	} else {
		after[op[3]] = 0
	}

	return after, true
}

func eqir(before, op []uint32) ([]uint32, bool) {
	if op[3] < 0 || op[3] > 3 {
		return nil, false
	}
	if op[2] < 0 || op[2] > 3 {
		return nil, false
	}
	after := make([]uint32, 4)
	copy(after, before)
	if op[1] == after[op[2]] {
		after[op[3]] = 1
	} else {
		after[op[3]] = 0
	}

	return after, true
}

func eqri(before, op []uint32) ([]uint32, bool) {
	if op[3] < 0 || op[3] > 3 {
		return nil, false
	}
	if op[1] < 0 || op[1] > 3 {
		return nil, false
	}
	after := make([]uint32, 4)
	copy(after, before)
	if before[op[1]] == op[2] {
		after[op[3]] = 1
	} else {
		after[op[3]] = 0
	}

	return after, true
}

func eqrr(before, op []uint32) ([]uint32, bool) {
	if op[3] < 0 || op[3] > 3 {
		return nil, false
	}
	if op[2] < 0 || op[2] > 3 {
		return nil, false
	}
	if op[1] < 0 || op[1] > 3 {
		return nil, false
	}
	after := make([]uint32, 4)
	copy(after, before)
	if before[op[1]] == after[op[2]] {
		after[op[3]] = 1
	} else {
		after[op[3]] = 0
	}

	return after, true
}
