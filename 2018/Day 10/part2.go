package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
	"time"
)

type point struct {
	px, py int
	vx, vy int
}

func main() {
	data, _ := ioutil.ReadFile("2018/day 10/input.txt")
	input := string(data)

	points := make([]point, 0)
	for _, line := range strings.Split(input, "\n") {
		var p point
		start := strings.Index(line, "<")
		stop := strings.Index(line, ">")
		position := strings.TrimSpace(line[start+1 : stop])
		pos := strings.Split(position, ",")
		start = strings.LastIndex(line, "<")
		stop = strings.LastIndex(line, ">")
		velocity := strings.TrimSpace(line[start+1 : stop])
		vel := strings.Split(velocity, ",")

		p.px, _ = strconv.Atoi(strings.TrimSpace(pos[0]))
		p.py, _ = strconv.Atoi(strings.TrimSpace(pos[1]))
		p.vx, _ = strconv.Atoi(strings.TrimSpace(vel[0]))
		p.vy, _ = strconv.Atoi(strings.TrimSpace(vel[1]))
		points = append(points, p)
	}
	fmt.Println(points)

	grid := make([][]string, 20)
	for i := range grid {
		grid[i] = make([]string, 72)
	}
	distX := 10000000000
	for n := 0; ; n++ {
		visible := false
		maxX := -100000000000000
		minX := 100000000000000
		var mx point
		var ux point
		for _, p := range points {
			if p.px >= 177 && p.px <= 248 && p.py >= 162 && p.py <= 181 {
				grid[(p.py)-162][(p.px)-177] = "#"
				visible = true
			}
			if p.px > maxX {
				maxX = p.px
				mx = p
			}
			if p.px < minX {
				minX = p.px
				ux = p
			}
		}
		if maxX-minX < distX {
			distX = maxX - minX
		} else {
			fmt.Println(distX)
			fmt.Println(n)
			fmt.Printf("%d - %d", mx.px, ux.px)
			return
		}

		if visible {
			for y := range grid {
				for x := range grid[y] {
					if grid[y][x] == "" {
						fmt.Print(".")
					}
					fmt.Print(grid[y][x])
				}
				fmt.Println("")
			}
			time.Sleep(1 * time.Second)
		}
		for i, p := range points {
			if p.px >= 177 && p.px <= 248 && p.py >= 162 && p.py <= 181 {
				grid[(p.py)-162][(p.px)-177] = "."
				visible = true
			}
			points[i].py += points[i].vy
			points[i].px += points[i].vx
		}
	}

}
