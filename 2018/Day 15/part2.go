package main

import (
	"container/list"
	"fmt"
	"io/ioutil"
	"sort"
	"strings"
)

const (
	WALL   = '#'
	SPACE  = '.'
	ELF    = 'E'
	GOBLIN = 'G'
)

var (
	grid [][]rune
)

type unit struct {
	x, y int
	kind rune
	hp   int
	dmg  int
	id   int
}

type unitSlice []*unit

func (u unitSlice) Sort() {
	sort.Sort(u)
}

func (u unitSlice) Len() int {
	return len(u)
}

func (u unitSlice) Less(i, j int) bool {
	if u[i].y < u[j].y {
		return true
	} else if u[i].y == u[j].y {
		return u[i].x < u[j].x
	}
	return false
}

func (u unitSlice) Swap(i, j int) {
	u[i], u[j] = u[j], u[i]
}

func main() {
	grid = make([][]rune, 0)
	goblins := make(unitSlice, 0)
	elves := make(unitSlice, 0)
	units := make(unitSlice, 0)

	data, _ := ioutil.ReadFile("2018/Day 15/input.txt")
	id := 0
	for y, line := range strings.Split(string(data), "\n") {
		grid = append(grid, make([]rune, 0))
		for x, c := range line {
			if c == ELF || c == GOBLIN {
				grid[y] = append(grid[y], SPACE)
				unit := &unit{x: x, y: y, hp: 200, dmg: 3, id: id}
				id++
				units = append(units, unit)
				if c == ELF {
					unit.kind = ELF
					unit.dmg = 25
					elves = append(elves, unit)
				} else {
					unit.kind = GOBLIN
					goblins = append(goblins, unit)
				}
			} else {
				grid[y] = append(grid[y], c)
			}
		}
	}
	units.Sort()
	print(units, 0)
	var rounds int
	for rounds = 0; len(goblins) != 0 && len(elves) != 0; rounds++ {
		units.Sort()
		for i, unit := range units {
			if unit.hp <= 0 {
				continue
			}
			emptySpots := make([]square, 0)
			for _, enemy := range units {
				if unit == enemy || unit.kind == enemy.kind || enemy.hp <= 0 {
					continue
				}
				emptySpots = append(emptySpots, unitNeightbours(square{x: enemy.x, y: enemy.y}, units, enemy, unit)...)
			}

			var target square
			target.dist = 1000000
			lowest := 200

			if len(emptySpots) == 0 {
				continue
			}
			for _, spot := range emptySpots {
				var s square
				s.x, s.y, s.dist = pathfind(unit.x, unit.y, spot.x, spot.y, units)
				if s.dist == -1 {
					continue
				}
				s.u = spot.u
				if s.dist < target.dist {
					target = s
				}
				if s.dist == 0 {
					if s.u.hp < lowest {
						lowest = s.u.hp
						target = s
					}
				}
			}
			if target.dist > 0 {
				newX, newY, dist := pathfind(unit.x, unit.y, target.x, target.y, units)
				if dist < 0 {
					continue
				} else if dist == 1 {
					target.dist = 1
				}
				units[i].x = newX
				units[i].y = newY
			}

			emptySpots = make([]square, 0)
			for _, enemy := range units {
				if unit == enemy || unit.kind == enemy.kind || enemy.hp <= 0 {
					continue
				}
				emptySpots = append(emptySpots, unitNeightbours(square{x: enemy.x, y: enemy.y}, units, enemy, unit)...)
			}

			target = square{}
			target.dist = 1000000
			lowest = 200

			if len(emptySpots) == 0 {
				continue
			}
			for _, spot := range emptySpots {
				var s square
				s.x, s.y, s.dist = pathfind(unit.x, unit.y, spot.x, spot.y, units)
				if s.dist == -1 {
					continue
				}
				s.u = spot.u
				if s.dist < target.dist {
					target = s
				}
				if s.dist == 0 {
					if s.u.hp < lowest {
						lowest = s.u.hp
						target = s
					}
				}
			}

			if target.dist == 0 {
				target.u.hp -= unit.dmg
				if target.u.hp <= 0 {
					if target.u.kind == ELF {
						fmt.Println("fail")
						return
						for i := range elves {
							if elves[i] == target.u {
								if i < len(elves) {
									elves = append(elves[:i], elves[i+1:]...)
									break
								} else {
									elves = elves[:i]
									break
								}
							}
						}
					} else {
						for i := range goblins {
							if goblins[i] == target.u {
								if i < len(goblins) {
									goblins = append(goblins[:i], goblins[i+1:]...)
									break
								} else {
									goblins = goblins[:i]
									break
								}
							}
						}
					}
				}
			}
		}
		print(units, rounds)
	}
}

func print(units unitSlice, rounds int) {
	units.Sort()
	fmt.Printf("   Round %d\n", rounds)
	for _, u := range units {
		if u.hp > 0 {
			fmt.Printf(" id: %d, %c: %d    %d,%d\n", u.id, u.kind, u.hp, u.x, u.y)
		}
	}
	for y, line := range grid {
		for x := range line {
			empty := true
			for _, unit := range units {
				if unit.x == x && unit.y == y && unit.hp > 0 {
					fmt.Printf("%c", unit.kind)
					empty = false
				}
			}
			if empty {
				fmt.Printf("%c", grid[y][x])
			}
		}
		fmt.Println("")
	}
	total := 0
	for _, u := range units {
		if u.hp > 0 {
			total += u.hp
		}
	}
	fmt.Printf("hp:%d   hp * rounds:%d\n\n", total, total*rounds)
}

func printPath(m map[square]square, units unitSlice) {
	for y, line := range grid {
		for x := range line {
			empty := true
			for k := range m {
				if k.x == x && k.y == y {
					fmt.Printf("%c", 'X')
					empty = false
					break
				}
			}

			for _, unit := range units {
				if unit.x == x && unit.y == y {
					fmt.Printf("%c", unit.kind)
					empty = false
				}
			}
			if empty {
				fmt.Printf("%c", grid[y][x])
			}
		}
		fmt.Println("")
	}
}

type square struct {
	x, y int
	dist int
	u    *unit
}

type squareSlice []square

func (u squareSlice) Sort() {
	sort.Sort(u)
}

func (u squareSlice) Len() int {
	return len(u)
}

func (u squareSlice) Less(i, j int) bool {
	if u[i].y < u[j].y {
		return true
	} else if u[i].y == u[j].y {
		return u[i].x < u[j].x
	}
	return false
}

func (u squareSlice) Swap(i, j int) {
	u[i], u[j] = u[j], u[i]
}

func pathfind(fromX, fromY, toX, toY int, units unitSlice) (x, y, dist int) {
	origin := square{x: fromX, y: fromY}
	end := square{x: toX, y: toY}

	if origin == end {
		return origin.x, origin.y, 0
	}

	l := list.New().Init()
	l.PushFront(origin)

	from := make(map[square]square)

	for l.Len() != 0 {
		s := l.Remove(l.Front()).(square)
		if s == end {
			prev := from[s]
			x = s.x
			y = s.y
			dist = 1
			for prev != origin {
				x = prev.x
				y = prev.y
				dist = dist + 1
				prev = from[prev]
			}
			return
		}
		for _, neighbour := range neighbours(s, units) {
			if _, ok := from[neighbour]; !ok {
				from[neighbour] = s
				l.PushBack(neighbour)
			}
		}
	}
	return -1, -1, -1
}

func unitNeightbours(s square, units unitSlice, e, u *unit) []square {
	var squares squareSlice

	if s.x+1 < len(grid[0]) {
		right := square{x: s.x + 1, y: s.y}
		if !checkCollision(right, units) || u.x == right.x && u.y == right.y {
			squares = append(squares, right)
		}
	}
	if s.x-1 >= 0 {
		left := square{x: s.x - 1, y: s.y}
		if !checkCollision(left, units) || u.x == left.x && u.y == left.y {
			squares = append(squares, left)
		}
	}
	if s.y-1 >= 0 {
		up := square{x: s.x, y: s.y - 1}
		if !checkCollision(up, units) || u.x == up.x && u.y == up.y {
			squares = append(squares, up)
		}
	}
	if s.y+1 < len(grid) {
		down := square{x: s.x, y: s.y + 1}

		if !checkCollision(down, units) || u.x == down.x && u.y == down.y {
			squares = append(squares, down)
		}

	}

	for i := range squares {
		squares[i].u = e
	}
	squares.Sort()
	return squares
}

func neighbours(s square, units unitSlice) []square {
	var squares squareSlice

	if s.x+1 < len(grid[0]) {
		right := square{x: s.x + 1, y: s.y}
		if !checkCollision(right, units) {
			squares = append(squares, right)
		}
	}
	if s.x-1 >= 0 {
		left := square{x: s.x - 1, y: s.y}
		if !checkCollision(left, units) {
			squares = append(squares, left)
		}
	}
	if s.y-1 >= 0 {
		up := square{x: s.x, y: s.y - 1}
		if !checkCollision(up, units) {
			squares = append(squares, up)
		}
	}
	if s.y+1 < len(grid) {
		down := square{x: s.x, y: s.y + 1}
		if !checkCollision(down, units) {
			squares = append(squares, down)
		}
	}

	squares.Sort()
	return squares
}

func checkCollision(s square, units unitSlice) bool {
	if grid[s.y][s.x] == SPACE {
		for _, unit := range units {
			if unit.hp <= 0 {
				continue
			}
			if unit.x == s.x && unit.y == s.y {
				return true
			}
		}
		return false
	} else {
		return true
	}
}
