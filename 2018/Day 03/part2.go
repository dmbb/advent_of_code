package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

func main() {
	dat, _ := ioutil.ReadFile("input.txt")

	lines := strings.Split(string(dat), "\n")

	m := make(map[string]int)
	for _, line := range lines {
		components := strings.Split(line, " ")
		coords := strings.Split(components[2][:len(components[2])-1], ",")
		size := strings.Split(components[3], "x")

		x, _ := strconv.Atoi(coords[0])
		width, _ := strconv.Atoi(size[0])
		y, _ := strconv.Atoi(coords[1])
		height, _ := strconv.Atoi(size[1])

		for i := 0; i < width; i++ {
			for j := 0; j < height; j++ {
				m[fmt.Sprintf("%d,%d", i+x, j+y)]++
			}
		}
	}
	for _, line := range lines {
		components := strings.Split(line, " ")
		coords := strings.Split(components[2][:len(components[2])-1], ",")
		size := strings.Split(components[3], "x")

		x, _ := strconv.Atoi(coords[0])
		width, _ := strconv.Atoi(size[0])
		y, _ := strconv.Atoi(coords[1])
		height, _ := strconv.Atoi(size[1])

		overlap := false
		for i := 0; i < width; i++ {
			for j := 0; j < height; j++ {
				if m[fmt.Sprintf("%d,%d", i+x, j+y)] > 1 {
					overlap = true
				}
			}
		}
		if !overlap {
			fmt.Println(line)
		}
	}
	var count int
	for _, v := range m {
		if v > 1 {
			count++
		}
	}

	fmt.Println(count)

}
