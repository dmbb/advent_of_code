package main

import (
	"fmt"
	"io/ioutil"
	"sort"
	"strings"
)

type order struct {
	before, after string
}
type letterSet []*letter

type letter struct {
	s      string
	before int
	done   bool
	n      letterSet
	b      letterSet
}

func (l letterSet) Sort() {
	sort.Sort(l)
}

func (l letterSet) Len() int {
	return len(l)
}

func (l letterSet) Less(i, j int) bool {
	return l[i].s < l[j].s
}

func (l letterSet) Swap(i, j int) {
	l[i], l[j] = l[j], l[i]
}

func main() {
	data, _ := ioutil.ReadFile("2018/Day 07/input.txt")
	input := string(data)

	orderSlice := make([]order, 0)
	m := make(map[string]*letter)
	for _, line := range strings.Split(input, "\n") {
		split := strings.Split(line, " ")
		m[split[1]] = &letter{s: split[1]}
		m[split[7]] = &letter{s: split[7]}
		orderSlice = append(orderSlice, order{before: split[1], after: split[7]})
	}

	for _, o := range orderSlice {
		m[o.before].n = append(m[o.before].n, m[o.after])
		m[o.after].b = append(m[o.after].b, m[o.before])
		m[o.after].before++
	}

	zero := letter{s: "~"}
	for _, v := range m {
		if v.before == 0 {
			zero.n = append(zero.n, v)
			v.b = append(v.b, &zero)
		}
	}
	zero.done = true

	next := make(letterSet, 0)
	next = append(next, zero.n...)

	for len(next) > 0 {
		next.Sort()
		if !next[0].done {
			for _, p := range next[0].b {
				if !p.done {
					goto Done
				}
			}
			next[0].done = true
			fmt.Print(next[0].s)
			next = append(next, next[0].n...)
		}
	Done:
		next = next[1:]
	}
}
