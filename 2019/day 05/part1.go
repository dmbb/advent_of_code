package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

func main() {
	data, _ := ioutil.ReadFile("input.txt")
	inst := make([]int, 0, 0)
	for _, number := range strings.Split(string(data), ",") {
		n, _ := strconv.Atoi(strings.TrimSpace(number))
		inst = append(inst, n)
	}

	input := 1

	for ip := 0; ip < len(inst); {
		code := inst[ip] % 100
		mode := make([]int, 3, 3)
		mode[0] = (inst[ip] / 100) % 10
		mode[1] = (inst[ip] / 1000) % 10
		mode[2] = (inst[ip] / 10000) % 10

		if code == 99 {
			break
		}

		param := make([]int, 3, 3)
		for i := 0; i < 3; i++ {
			switch mode[i] {
			case 0: // Position
				param[i] = inst[ip+i+1]
			case 1: // Immediate
				param[i] = ip + i + 1
			}
		}

		switch code {
		case 1:
			inst[param[2]] = inst[param[0]] + inst[param[1]]
			ip += 4
		case 2:
			inst[param[2]] = inst[param[0]] * inst[param[1]]
			ip += 4
		case 3:
			inst[param[0]] = input
			ip += 2
		case 4:
			fmt.Println(inst[param[0]])
			ip += 2
		default:
			goto Done
		}
	}
Done:
}
