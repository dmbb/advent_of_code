package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

func main() {
	data, _ := ioutil.ReadFile("input")
	modules := make([]int, 0, 0)
	for _, number := range strings.Split(string(data), "\n") {
		n, _ := strconv.Atoi(strings.TrimSpace(number))
		modules = append(modules, n)
	}
	var total int
	for _, m := range modules {
		fuel := (m / 3) - 2
		total += fuel

		for {
			fuel = (fuel / 3) - 2
			if fuel < 0 {
				break
			}
			total += fuel
		}
	}

	fmt.Println(total)
}
