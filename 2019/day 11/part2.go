package main

import (
	"fmt"
	"io/ioutil"
	"math"
	"strconv"
	"strings"
)

func main() {
	data, _ := ioutil.ReadFile("input.txt")
	split := strings.Split(string(data), ",")

	mem := make([]int, len(split))
	for i, s := range split {
		mem[i], _ = strconv.Atoi(s)
	}

	in := make(chan int, 1)
	out := make(chan int, 2)
	done := make(chan struct{})

	go run(mem, in, out, done)

	m := make(map[string]int)
	x := 0
	y := 0
	d := 0
	m[fmt.Sprintf("%v,%v", x, y)] = 1
out:
	for {
		select {
		case <-done:
			break out
		default:
			k := fmt.Sprintf("%v,%v", x, y)
			in <- m[k]

			c := <-out
			t := <-out

			m[k] = c
			if t == 0 {
				d = ((d - 1) + 4) % 4
			} else {
				d = (d + 1) % 4
			}
			switch d {
			case 0:
				y++
			case 1:
				x++
			case 2:
				y--
			case 3:
				x--
			}
		}
	}
	maxx, maxy, minx, miny := 0, 0, 0, 0
	for k := range m {
		s := strings.Split(k, ",")
		x, _ := strconv.Atoi(s[0])
		y, _ := strconv.Atoi(s[1])

		if x > maxx {
			maxx = x
		}
		if y > maxy {
			maxy = y
		}

		if x < minx {
			minx = x
		}
		if y < minx {
			miny = y
		}
	}

	for y := maxy; y >= miny; y-- {
		for x := minx; x <= maxx; x++ {
			k := fmt.Sprintf("%v,%v", x, y)
			if m[k] == 0 {
				fmt.Print(" ")
			} else {
				fmt.Print("#")
			}
		}
		fmt.Println()
	}
}

func run(inst []int, in, out chan int, done chan struct{}) {
	mem := append([]int{}, inst...)
	for i := 0; i < 10000; i++ {
		mem = append(mem, 0)
	}
	ip := 0
	rel := 0

	for {
		arg := func(p float64) int {
			switch (mem[ip] / (int(math.Pow(10, p) * 10))) % 10 {
			case 0:
				return mem[ip+int(p)]
			case 1:
				return ip + int(p)
			case 2:
				return rel + mem[ip+int(p)]
			default:
				return 0
			}
		}

		code := mem[ip] % 100
		switch code {
		case 1:
			mem[arg(3)] = mem[arg(1)] + mem[arg(2)]
		case 2:
			mem[arg(3)] = mem[arg(1)] * mem[arg(2)]
		case 3:
			mem[arg(1)] = <-in
		case 4:
			out <- mem[arg(1)]
		case 5:
			if mem[arg(1)] != 0 {
				ip = mem[arg(2)]
				continue
			}
		case 6:
			if mem[arg(1)] == 0 {
				ip = mem[arg(2)]
				continue
			}
		case 7:
			if mem[arg(1)] < mem[arg(2)] {
				mem[arg(3)] = 1
			} else {
				mem[arg(3)] = 0
			}
		case 8:
			if mem[arg(1)] == mem[arg(2)] {
				mem[arg(3)] = 1
			} else {
				mem[arg(3)] = 0
			}
		case 9:
			rel = rel + mem[arg(1)]
		case 99:
			fallthrough
		default:
			done <- struct{}{}
			return
		}

		ip += []int{1, 4, 4, 2, 2, 3, 3, 4, 4, 2}[code]
	}
}
