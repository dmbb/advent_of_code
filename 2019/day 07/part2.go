package main

import (
	"fmt"
	"io/ioutil"
	"math"
	"strconv"
	"strings"
)

func main() {
	data, _ := ioutil.ReadFile("input.txt")
	split := strings.Split(string(data), ",")

	mem := make([]int, len(split))
	for i, s := range split {
		mem[i], _ = strconv.Atoi(s)
	}
	mem = append(mem, 0)

	phase := []int{5, 6, 7, 8, 9}

	max := 0
	for _, p := range permutations(phase) {
		done := make(chan struct{})
		var c []chan int
		for range phase {
			c = append(c, make(chan int, 1))
		}

		for i := range c {
			runAmp(p[i], mem, c[i], c[(i+1)%len(c)], done)
		}

		c[0] <- 0
		for range phase {
			<-done
		}

		out := <-c[0]
		if out > max {
			max = out
		}
	}
	fmt.Println(max)
}

func permutations(nums []int) [][]int {
	var out [][]int

	if len(nums) == 1 {
		return [][]int{nums}
	}

	for i := range nums {
		cpy := append([]int{}, nums...)
		for _, p := range permutations(append(cpy[:i], cpy[i+1:]...)) {
			out = append(out, append([]int{nums[i]}, p...))
		}
	}
	return out
}

func runAmp(phase int, inst []int, in, out chan int, done chan struct{}) {
	go func() {
		mem := append([]int{}, inst...)
		ip := 0
		init := true

		for {
			arg := func(p float64) int {
				if (mem[ip]/(int(math.Pow(10, p)*10)))%10 == 0 {
					return mem[mem[ip+int(p)]]
				}
				return mem[ip+int(p)]
			}

			code := mem[ip] % 100
			switch code {
			case 1:
				mem[mem[ip+3]] = arg(1) + arg(2)
			case 2:
				mem[mem[ip+3]] = arg(1) * arg(2)
			case 3:
				if init {
					mem[mem[ip+1]] = phase
					init = false
				} else {
					mem[mem[ip+1]] = <-in
				}
			case 4:
				out <- arg(1)
			case 5:
				if arg(1) != 0 {
					ip = arg(2)
					continue
				}
			case 6:
				if arg(1) == 0 {
					ip = arg(2)
					continue
				}
			case 7:
				if arg(1) < arg(2) {
					mem[mem[ip+3]] = 1
				} else {
					mem[mem[ip+3]] = 0
				}
			case 8:
				if arg(1) == arg(2) {
					mem[mem[ip+3]] = 1
				} else {
					mem[mem[ip+3]] = 0
				}
			case 99:
				fallthrough
			default:
				done <- struct{}{}
				return
			}

			ip += []int{1, 4, 4, 2, 2, 3, 3, 4, 4}[code]
		}
	}()
}
