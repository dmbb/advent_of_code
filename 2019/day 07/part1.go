package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

func main() {
	max := 0
	vals := []int{0, 0, 0, 0, 0}
	for a := 0; a < 5; a++ {
		for b := 0; b < 5; b++ {
			if b == a {
				continue
			}
			for c := 0; c < 5; c++ {
				if c == b || c == a {
					continue
				}
				for d := 0; d < 5; d++ {
					if d == c || d == b || d == a {
						continue
					}
					for e := 0; e < 5; e++ {
						if e == d || e == c || e == b || e == a {
							continue
						}
						out := amplifier(a, 0)
						out = amplifier(b, out)
						out = amplifier(c, out)
						out = amplifier(d, out)
						out = amplifier(e, out)
						if out > max {
							max = out
							vals[0] = a
							vals[1] = b
							vals[2] = c
							vals[3] = d
							vals[4] = e
						}
					}
				}
			}
		}
	}
	fmt.Println(max)
	fmt.Println(vals)
}

func amplifier(phase, input int) int {
	data, _ := ioutil.ReadFile("input.txt")
	inst := make([]int, 0, 0)
	for _, number := range strings.Split(string(data), ",") {
		n, _ := strconv.Atoi(strings.TrimSpace(number))
		inst = append(inst, n)
	}

	first := true
	for ip := 0; ip < len(inst); {
		code := inst[ip] % 100
		mode := make([]int, 3, 3)
		mode[0] = (inst[ip] / 100) % 10
		mode[1] = (inst[ip] / 1000) % 10
		mode[2] = (inst[ip] / 10000) % 10

		if code == 99 {
			break
		}

		param := make([]int, 3, 3)
		for i := 0; i < 3; i++ {
			switch mode[i] {
			case 0: // Position
				param[i] = inst[ip+i+1]
			case 1: // Immediate
				param[i] = ip + i + 1
			}
		}

		switch code {
		case 1:
			inst[param[2]] = inst[param[0]] + inst[param[1]]
			ip += 4
		case 2:
			inst[param[2]] = inst[param[0]] * inst[param[1]]
			ip += 4
		case 3:
			if first {
				inst[param[0]] = phase
				first = false
			} else {
				inst[param[0]] = input
			}
			ip += 2
		case 4:
			return inst[param[0]]
		case 5:
			if inst[param[0]] != 0 {
				ip = inst[param[1]]
			} else {
				ip += 3
			}
		case 6:
			if inst[param[0]] == 0 {
				ip = inst[param[1]]
			} else {
				ip += 3
			}
		case 7:
			if inst[param[0]] < inst[param[1]] {
				inst[param[2]] = 1
			} else {
				inst[param[2]] = 0
			}
			ip += 4
		case 8:
			if inst[param[0]] == inst[param[1]] {
				inst[param[2]] = 1
			} else {
				inst[param[2]] = 0
			}
			ip += 4
		default:
		}
	}
	return 0
}
