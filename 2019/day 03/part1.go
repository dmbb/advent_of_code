package main

import (
	"fmt"
	"io/ioutil"
	"math"
	"strconv"
	"strings"
)

func main() {
	data, _ := ioutil.ReadFile("input.txt")

	grid := make([][]int, 50000)
	for y := range grid {
		grid[y] = make([]int, 50000)
	}

	wire := strings.Split(string(data), "\n")[0]
	prevX, prevY := 25000, 25000
	for _, seg := range strings.Split(wire, ",") {
		length, _ := strconv.Atoi(seg[1:])
		switch seg[0] {
		case 'R':
			for x := prevX + 1; x <= prevX+length; x++ {
				grid[x][prevY] = 1
			}
			prevX = prevX + length
		case 'L':
			for x := prevX - 1; x >= prevX-length; x-- {
				grid[x][prevY] = 1
			}
			prevX = prevX - length
		case 'U':
			for y := prevY + 1; y <= prevY+length; y++ {
				grid[prevX][y] = 1
			}
			prevY = prevY + length
		case 'D':
			for y := prevY - 1; y >= prevY-length; y-- {
				grid[prevX][y] = 1
			}
			prevY = prevY - length
		}
	}
	wire = strings.Split(string(data), "\n")[1]
	prevX, prevY = 25000, 25000

	smallest := 9999999999.0

	for _, seg := range strings.Split(wire, ",") {
		length, _ := strconv.Atoi(seg[1:])
		switch seg[0] {
		case 'R':
			for x := prevX + 1; x <= prevX+length; x++ {
				if grid[x][prevY] == 1 {
					m := math.Abs(float64(x-25000)) + math.Abs(float64(prevY-25000))
					if m < smallest {
						smallest = m
					}
				}
			}
			prevX = prevX + length
		case 'L':
			for x := prevX - 1; x >= prevX-length; x-- {
				if grid[x][prevY] == 1 {
					m := math.Abs(float64(x-25000)) + math.Abs(float64(prevY-25000))
					if m < smallest {
						smallest = m
					}
				}
			}
			prevX = prevX - length
		case 'U':
			for y := prevY + 1; y <= prevY+length; y++ {
				if grid[prevX][y] == 1 {
					m := math.Abs(float64(y-25000)) + math.Abs(float64(prevX-25000))
					if m < smallest {
						smallest = m
					}
				}
			}
			prevY = prevY + length
		case 'D':
			for y := prevY - 1; y >= prevY-length; y-- {
				if grid[prevX][y] == 1 {
					m := math.Abs(float64(y-25000)) + math.Abs(float64(prevX-25000))
					if m < smallest {
						smallest = m
					}
				}
			}
			prevY = prevY - length
		}
	}
	fmt.Println(smallest)
}
