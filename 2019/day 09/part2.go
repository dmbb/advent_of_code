package main

import (
	"fmt"
	"io/ioutil"
	"math"
	"strconv"
	"strings"
)

func main() {
	data, _ := ioutil.ReadFile("input.txt")
	split := strings.Split(string(data), ",")

	mem := make([]int, len(split))
	for i, s := range split {
		mem[i], _ = strconv.Atoi(s)
	}
	for i := 0; i < 10000; i++ {
		mem = append(mem, 0)
	}

	mode := 2
	run(mode, mem)
}

func run(mode int, inst []int) {
	mem := append([]int{}, inst...)
	ip := 0
	rel := 0

	for {
		arg := func(p float64) int {
			switch (mem[ip] / (int(math.Pow(10, p) * 10))) % 10 {
			case 0:
				return mem[ip+int(p)]
			case 1:
				return ip + int(p)
			case 2:
				return rel + mem[ip+int(p)]
			default:
				return 0
			}
		}

		code := mem[ip] % 100
		switch code {
		case 1:
			mem[arg(3)] = mem[arg(1)] + mem[arg(2)]
		case 2:
			mem[arg(3)] = mem[arg(1)] * mem[arg(2)]
		case 3:
			mem[arg(1)] = mode
		case 4:
			fmt.Println(mem[arg(1)])
		case 5:
			if mem[arg(1)] != 0 {
				ip = mem[arg(2)]
				continue
			}
		case 6:
			if mem[arg(1)] == 0 {
				ip = mem[arg(2)]
				continue
			}
		case 7:
			if mem[arg(1)] < mem[arg(2)] {
				mem[arg(3)] = 1
			} else {
				mem[arg(3)] = 0
			}
		case 8:
			if mem[arg(1)] == mem[arg(2)] {
				mem[arg(3)] = 1
			} else {
				mem[arg(3)] = 0
			}
		case 9:
			rel = rel + mem[arg(1)]
		case 99:
			fallthrough
		default:
			return
		}

		ip += []int{1, 4, 4, 2, 2, 3, 3, 4, 4, 2}[code]
	}
}
