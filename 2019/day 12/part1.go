package main

import (
	"fmt"
	"io/ioutil"
	"math"
	"strconv"
	"strings"
)

type coord struct {
	x, y, z int
}

func (c coord) energy() int {
	return int(math.Abs(float64(c.x)) + math.Abs(float64(c.y)) + math.Abs(float64(c.z)))
}

type planet struct {
	pos, vel coord
}

func (p *planet) energy() int {
	return p.pos.energy() * p.vel.energy()
}

func (p *planet) gravity(o *planet) {
	if o == p {
		return
	}

	if p.pos.x < o.pos.x {
		p.vel.x++
		o.vel.x--
	} else if p.pos.x > o.pos.x {
		p.vel.x--
		o.vel.x++
	}

	if p.pos.y < o.pos.y {
		p.vel.y++
		o.vel.y--
	} else if p.pos.y > o.pos.y {
		p.vel.y--
		o.vel.y++
	}

	if p.pos.z < o.pos.z {
		p.vel.z++
		o.vel.z--
	} else if p.pos.z > o.pos.z {
		p.vel.z--
		o.vel.z++
	}
}

func (p *planet) move() {
	p.pos.x += p.vel.x
	p.pos.y += p.vel.y
	p.pos.z += p.vel.z
}

func main() {
	data, _ := ioutil.ReadFile("input.txt")
	raw := strings.Split(string(data), "\n")

	planets := make([]*planet, 0)
	for _, l := range raw {
		v := strings.Split(strings.TrimSpace(l), ", ")
		p := planet{}
		p.pos.x, _ = strconv.Atoi(v[0][3:])
		p.pos.y, _ = strconv.Atoi(v[1][2:])
		p.pos.z, _ = strconv.Atoi(v[2][2 : len(v[2])-1])
		planets = append(planets, &p)
	}

	maxSteps := 10000

	for step := 0; step < maxSteps; step++ {
		for i, p := range planets {
			for j := i; j < len(planets); j++ {
				p.gravity(planets[j])
			}
		}

		for _, p := range planets {
			p.move()
		}
	}

	e := 0
	for _, p := range planets {
		e += p.energy()
	}
	fmt.Println(e)
}
