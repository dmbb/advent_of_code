package main

import (
	"fmt"
	"io/ioutil"
	"math"
	"strconv"
	"strings"
)

type coord struct {
	x, y, z int
}

func (c coord) energy() int {
	return int(math.Abs(float64(c.x)) + math.Abs(float64(c.y)) + math.Abs(float64(c.z)))
}

type planet struct {
	pos, ini, vel, fre coord

	frequency int
}

func (p *planet) energy() int {
	return p.pos.energy() * p.vel.energy()
}

func (p *planet) gravity(o *planet) {
	if o == p {
		return
	}

	if p.pos.x < o.pos.x {
		p.vel.x++
		o.vel.x--
	} else if p.pos.x > o.pos.x {
		p.vel.x--
		o.vel.x++
	}

	if p.pos.y < o.pos.y {
		p.vel.y++
		o.vel.y--
	} else if p.pos.y > o.pos.y {
		p.vel.y--
		o.vel.y++
	}

	if p.pos.z < o.pos.z {
		p.vel.z++
		o.vel.z--
	} else if p.pos.z > o.pos.z {
		p.vel.z--
		o.vel.z++
	}
}

func (p *planet) move() {
	p.pos.x += p.vel.x
	p.pos.y += p.vel.y
	p.pos.z += p.vel.z
}

func main() {
	data, _ := ioutil.ReadFile("input.txt")
	raw := strings.Split(string(data), "\n")

	planets := make([]*planet, 0)
	for _, l := range raw {
		v := strings.Split(strings.TrimSpace(l), ", ")
		p := planet{}
		p.pos.x, _ = strconv.Atoi(v[0][3:])
		p.pos.y, _ = strconv.Atoi(v[1][2:])
		p.pos.z, _ = strconv.Atoi(v[2][2 : len(v[2])-1])
		p.ini.x, p.ini.y, p.ini.z = p.pos.x, p.pos.y, p.pos.z

		planets = append(planets, &p)
	}

	freqX := 0
	freqY := 0
	freqZ := 0
	for step := 1; ; step++ {
		for i, p := range planets {
			for j := i; j < len(planets); j++ {
				p.gravity(planets[j])
			}
		}

		for _, p := range planets {
			p.move()
		}

		if planets[0].pos.x == planets[0].ini.x && planets[0].vel.x == 0 &&
			planets[1].pos.x == planets[1].ini.x && planets[1].vel.x == 0 &&
			planets[2].pos.x == planets[2].ini.x && planets[2].vel.x == 0 &&
			planets[3].pos.x == planets[3].ini.x && planets[3].vel.x == 0 &&
			freqX == 0 {
			freqX = step
			fmt.Printf("x: %v\n", freqX)
		}

		if planets[0].pos.y == planets[0].ini.y && planets[0].vel.y == 0 &&
			planets[1].pos.y == planets[1].ini.y && planets[1].vel.y == 0 &&
			planets[2].pos.y == planets[2].ini.y && planets[2].vel.y == 0 &&
			planets[3].pos.y == planets[3].ini.y && planets[3].vel.y == 0 &&
			freqY == 0 {
			freqY = step
			fmt.Printf("y: %v\n", freqY)
		}

		if planets[0].pos.z == planets[0].ini.z && planets[0].vel.z == 0 &&
			planets[1].pos.z == planets[1].ini.z && planets[1].vel.z == 0 &&
			planets[2].pos.z == planets[2].ini.z && planets[2].vel.z == 0 &&
			planets[3].pos.z == planets[3].ini.z && planets[3].vel.z == 0 &&
			freqZ == 0 {
			freqZ = step
			fmt.Printf("z: %v\n", freqZ)
		}
		if freqX != 0 && freqY != 0 && freqZ != 0 {
			fmt.Println(lcm(freqX, freqY, freqZ))
			return
		}
	}
}

func lcm(a, b int, ints ...int) int {
	result := a * b / gcd(a, b)

	for i := 0; i < len(ints); i++ {
		result = lcm(result, ints[i])
	}

	return result
}

func gcd(a, b int) int {
	for b != 0 {
		t := b
		b = a % b
		a = t
	}
	return a
}
