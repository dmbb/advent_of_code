package main

import (
	"fmt"
	"io/ioutil"
	"strings"
)

type node struct {
	parent   *node
	name     string
	children []*node
}

func main() {
	data, _ := ioutil.ReadFile("input.txt")
	m := make(map[string]*node)
	for _, line := range strings.Split(string(data), "\n") {
		kv := strings.Split(strings.TrimSpace(line), ")")
		if m[kv[0]] == nil {
			m[kv[0]] = &node{name: kv[0], children: make([]*node, 0, 0)}
		}
		if m[kv[1]] == nil {
			m[kv[1]] = &node{name: kv[1], children: make([]*node, 0, 0)}
		}

		m[kv[0]].children = append(m[kv[0]].children, m[kv[1]])
		m[kv[1]].parent = m[kv[0]]
	}

	total := 0

	for _, v := range m {
		for v.parent != nil {
			v = v.parent
			total++
		}
	}

	fmt.Println(total)
}
