package main

import (
	"fmt"
	"strconv"
)

func main() {
	min := 172851
	max := 675869

	total := 0
	for n := min; n <= max; n++ {
		s := strconv.Itoa(n)
		if valid(s) {
			total++
		}
	}
	fmt.Println(total)
}

func valid(s string) bool {
	double := false
	for i := range s {
		if i == 0 {
			continue
		}

		if s[i] < s[i-1] {
			return false
		}

		if s[i] == s[i-1] {
			double = true
		}
	}
	return double
}
