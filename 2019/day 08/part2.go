package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
)

func main() {
	data, _ := ioutil.ReadFile("input.txt")
	mem := make([]int, len(string(data)))
	for i, s := range string(data) {
		mem[i], _ = strconv.Atoi(string(s))
	}
	layerSize := 25 * 6

	img := make([]int, layerSize)
	for i := range img {
		img[i] = 2
	}

	for layer := 0; layer*layerSize < len(mem); layer++ {
		for i := 0; i < layerSize; i++ {
			if img[i] == 2 {
				img[i] = mem[(layer*layerSize)+i]
			}
		}
	}
	for i := range img {
		if i%25 == 0 {
			fmt.Println()
		}
		if img[i] == 0 {
			fmt.Printf(" ")
		} else {
			fmt.Printf("%v", img[i])
		}
	}
}
