package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
)

func main() {
	data, _ := ioutil.ReadFile("input.txt")
	mem := make([]int, len(string(data)))
	for i, s := range string(data) {
		mem[i], _ = strconv.Atoi(string(s))
	}
	layerSize := 25 * 6

	zero := 25 * 6
	val := 0
	for layer := 0; layer*layerSize < len(mem); layer++ {
		m := make(map[int]int)
		for i := 0; i < layerSize; i++ {
			m[mem[(layer*layerSize)+i]]++
		}
		if m[0] < zero {
			zero = m[0]
			val = m[1] * m[2]
		}
	}
	fmt.Println(val)
}
