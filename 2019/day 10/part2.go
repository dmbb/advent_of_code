package main

import (
	"fmt"
	"io/ioutil"
	"math"
	"sort"
	"strconv"
	"strings"
)

func main() {
	data, _ := ioutil.ReadFile("input.txt")
	split := strings.Split(string(data), "\n")
	for i, l := range split {
		split[i] = strings.TrimSpace(l)
	}

	max := 0
	maxx := 0
	maxy := 0
	collisionMap := make(map[string]int)
	angleMap := make(map[float64][]string)
	for y, l := range split {
		for x, r := range l {
			if r == '.' {
				continue
			}

			m := make(map[string]int)
			a := make(map[float64][]string)
			for y2, l := range split {
				for x2, r := range l {
					if r == '.' || (y2 == y && x2 == x) {
						continue
					}
					s := fmt.Sprintf("%v,%v", (x-x2)/gcd(y-y2, x-x2), (y-y2)/gcd(y-y2, x-x2))
					angle := math.Atan2(float64(y2-y), float64(x2-x))/(2*math.Pi)*360.0 + 90
					if angle < 0 {
						angle += 360
					}
					a[angle] = append(a[angle], fmt.Sprintf("%v,%v", x2, y2))

					if m[s] > 0 {
						continue
					}
					m[s]++
				}
			}

			if len(m) > max {
				max = len(m)
				maxx = x
				maxy = y
				collisionMap = make(map[string]int)
				for k, v := range m {
					collisionMap[k] = v
				}
				angleMap = make(map[float64][]string)
				for k, v := range a {
					angleMap[k] = append(angleMap[k], v...)
				}
			}
		}
	}
	fmt.Println(max)
	fmt.Println(maxx)
	fmt.Println(maxy)

	s := make([]float64, 0)
	for k := range angleMap {
		s = append(s, k)
	}
	sort.Float64s(s)

	for k, v := range angleMap {
		c := make([]string, len(v))
		copy(c, v)

		sort.Slice(c, func(i, j int) bool {
			first := strings.Split(c[i], ",")
			second := strings.Split(c[j], ",")
			x1, _ := strconv.Atoi(first[0])
			y1, _ := strconv.Atoi(first[1])
			x2, _ := strconv.Atoi(second[0])
			y2, _ := strconv.Atoi(second[1])

			d1 := math.Abs(float64(x1-maxx)) + math.Abs(float64(y1-maxy))
			d2 := math.Abs(float64(x2-maxx)) + math.Abs(float64(y2-maxy))

			return d1 < d2
		})
		copy(angleMap[k], c)
	}
	for _, a := range s {
		fmt.Printf("%v: %v\n", a, angleMap[a])
	}

	fmt.Println(len(s))
	for count := 0; count < 30; {
		fmt.Println("r")
		for _, a := range s {
			if len(angleMap[a]) == 0 {
				continue
			}
			count++
			fmt.Printf("%v\t%v\t%v\n", count, a, angleMap[a][0])
			angleMap[a] = angleMap[a][1:]
		}
	}

	/*
		for y, l := range split {
			for x, r := range l {
				if r == '.' {
					fmt.Printf("%c", r)
					continue
				}
				if m2[fmt.Sprintf("%v,%v", x, y)] > 0 {
					fmt.Printf("*")
					continue
				}
				if x == 5 && y == 8 {
					fmt.Print("#")
					continue
				}
				fmt.Print("/")
			}
			fmt.Println()
		}

	*/

}

func gcd(a, b int) int {
	for b != 0 {
		t := b
		b = a % b
		a = t
	}
	if a < 0 {
		a = a * -1
	}
	return a
}
