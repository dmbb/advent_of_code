package main

import (
	"fmt"
	"io/ioutil"
	"strings"
)

func main() {
	data, _ := ioutil.ReadFile("input.txt")
	split := strings.Split(string(data), "\n")
	for i, l := range split {
		split[i] = strings.TrimSpace(l)
	}

	max := 0
	maxx := 0
	maxy := 0
	for y, l := range split {
		for x, r := range l {
			if r == '.' {
				continue
			}

			m := make(map[string]int)

			for y2, l := range split {
				for x2, r := range l {
					if r == '.' || (y2 == y && x2 == x) {
						continue
					}
					s := fmt.Sprintf("%v,%v", (x-x2)/gcd(y-y2, x-x2), (y-y2)/gcd(y-y2, x-x2))
					if m[s] > 0 {
						continue
					}
					m[s]++
				}
			}

			if len(m) > max {
				max = len(m)
				maxx = x
				maxy = y
			}
		}
	}
	fmt.Println(max)
	fmt.Println(maxx)
	fmt.Println(maxy)

	/*
		for y, l := range split {
			for x, r := range l {
				if r == '.' {
					fmt.Printf("%c", r)
					continue
				}
				if m2[fmt.Sprintf("%v,%v", x, y)] > 0 {
					fmt.Printf("*")
					continue
				}
				if x == 5 && y == 8 {
					fmt.Print("#")
					continue
				}
				fmt.Print("/")
			}
			fmt.Println()
		}

	*/

}

func gcd(a, b int) int {
	for b != 0 {
		t := b
		b = a % b
		a = t
	}
	if a < 0 {
		a = a * -1
	}
	return a
}
