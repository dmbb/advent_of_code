package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

func main() {
	data, _ := ioutil.ReadFile("input.txt")
	inst := make([]int, 0, 0)
	for _, number := range strings.Split(string(data), ",") {
		n, _ := strconv.Atoi(strings.TrimSpace(number))
		inst = append(inst, n)
	}

	inst[1] = 12
	inst[2] = 2

	for i := 0; i < len(inst); {
		switch inst[i] {
		case 1:
			inst[inst[i+3]] = inst[inst[i+1]] + inst[inst[i+2]]
		case 2:
			inst[inst[i+3]] = inst[inst[i+1]] * inst[inst[i+2]]
		case 99:
			fallthrough
		default:
			break
		}
		i += 4
	}
	fmt.Println(inst[0])
}
