package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

func main() {
	data, _ := ioutil.ReadFile("input.txt")
	initial := make([]int, 149, 149)
	for i, number := range strings.Split(string(data), ",") {
		n, _ := strconv.Atoi(strings.TrimSpace(number))
		initial[i] = n
	}
	inst := make([]int, 149, 149)

	for x := 0; x < 100; x++ {
		for y := 0; y < 100; y++ {
			for i, v := range initial {
				inst[i] = v
			}

			inst[1] = x
			inst[2] = y

			for ip := 0; ip < len(inst); {
				switch inst[ip] {
				case 1:
					inst[inst[ip+3]] = inst[inst[ip+1]] + inst[inst[ip+2]]
					ip += 4
				case 2:
					inst[inst[ip+3]] = inst[inst[ip+1]] * inst[inst[ip+2]]
					ip += 4
				case 99:
					fallthrough
				default:
					if inst[0] == 19690720 {
						fmt.Println(x)
						fmt.Println(y)
						goto End
					} else {
						fmt.Printf("not %v %v\n", x, y)
					}
					goto Done
				}
			}
		Done:
		}
	}
End:
}
